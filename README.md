MyCC
====

![status](https://gitlab.com/capella/mycc/badges/master/pipeline.svg)

This is a compiler written in C++ as part of the MAC5750 class - Compilers
Theory and Building (University of São Paulo).

Why I keep this project? It was the best class during university and is good
summary of what a learned during the course. Also, the professor responsible
for the discipline was one of the most important ones in my life.

### Running

It is simple to, build the compiler:

```
make
```

At the moment, it only works on Linux x86. If you are running in odd system, you
can use the Dockerfile.

To generate an assembly run:
```
./mycc <PROGRAM_NAME> > out.s
```

There is still some work to do the transcript the assembly to machine code. You
can achieve by using `gcc`:

```
gcc out.s
```

### Notes

Using `MYCC_NUMBER_OF_REGISTERS` env var you can set the number of register used
by the final code. At the moment, it can be a value between 2 and 12. The
default register are: rbp, rsp, rdi, rax, rbx 

### Steps

The compiler doesn't use any external dependency, just pure C++. The parser
and the lexical analyser (lex) were written from the scratch.

The lex and the parser are build together with the compiler. The tokens are
defined in ``final/lex.lex`` and the grammar in ``final/parser``.

Depending on the flags this program can generate a Graphviz representation of
the internal state and the transformations.

### Directories

- ``final``: contains the compiler core
- ``lex``: lexical analyser
- ``parser``: grammar parser
- ``programs``: examples and expected results
- ``scripts``: helper scripts to build this site and run tests
- ``site``: this site in Hugo

### TODOs

Memory leaks? Yes, it is the default. Arrays? Not supported. However, you can
create objects.

