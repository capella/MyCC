////////////////////////////////////////////////////////////// */
//
// Author: Gabriel Capella
// Date: 2015-10-13
//
////////////////////////////////////////////////////////////// */
#include "firstandfollow.h"

bool FirstFollow::allnuable(Production &tokens) {
    return allnuable(0, tokens.size() - 1, tokens);
}

bool FirstFollow::allnuable(int i, int j, Production &tokens) {
    bool verify = 1;
    while (i <= j && verify) {
        verify &= nuable[tokens[i]];
        i++;
    }
    return verify;
}

FirstFollow::FirstFollow(map<string, int> &name_id, int firstnot,
                         Productions *productions) {
    //-------------------------------------------- First Folow nuable
    int changes = 1;
    first.resize(name_id.size());
    follow.resize(name_id.size());
    nuable.resize(name_id.size());

    firstnoterminal = firstnot;

    for (int i = 0; i < firstnot; i++) {
        first[i].insert(i);
    }

    changes = 1;
    while (changes) {
        changes = 0;

        // EACH PRODUCTION
        Productions::iterator it;
        for (it = productions->begin(); it != productions->end();
             it++) {
            vector<Production *> prods = it->second;
            for (unsigned int i = 0; i < prods.size(); i++) {
                int X = it->first;

                Production tokens = *(prods.at(i));
                unsigned int k = tokens.size();

                bool all_nuable = allnuable(tokens);
                if (all_nuable || k == 0) {
                    if (!nuable[X]) changes++;
                    nuable[X] = true;
                }

                for (unsigned int i = 0; i < k; ++i) {
                    int Yi = tokens[i];
                    if (allnuable(0, i - 1, tokens) || i == 0) {
                        unsigned int bsize = first[X].size();
                        first[X].insert(first[Yi].begin(),
                                        first[Yi].end());
                        if (bsize != first[X].size()) changes++;
                    }
                    if (allnuable(i + 1, k - 1, tokens) ||
                        i == k - 1) {
                        unsigned int bsize = follow[Yi].size();
                        follow[Yi].insert(follow[X].begin(),
                                          follow[X].end());
                        if (bsize != follow[Yi].size()) changes++;
                    }

                    for (unsigned int j = i + 1; j < k; ++j) {
                        int Yj = tokens[j];
                        if (allnuable(i + 1, j - 1, tokens) ||
                            i + 1 == j) {
                            unsigned int bsize = follow[Yi].size();
                            follow[Yi].insert(first[Yj].begin(),
                                              first[Yj].end());
                            if (bsize != follow[Yi].size()) changes++;
                        }
                    }
                }
            }
        }
    }
}

set<int> FirstFollow::firststring(int c, vector<int> &s) {
    int X = s[c];

    set<int> union_first = first[X];

    if (!nuable[X]) return union_first;

    set<int> firstXlo = firststring(c + 1, s);
    union_first.insert(firstXlo.begin(), firstXlo.end());

    return union_first;
}

vector<int> FirstFollow::firststring(vector<int> s) {
    vector<int> result;
    set<int> tmp = firststring(0, s);

    set<int>::iterator it;
    for (it = tmp.begin(); it != tmp.end(); it++)
        result.push_back((int)*it);
    return result;
}
