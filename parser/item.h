////////////////////////////////////////////////////////////// */
//
// Author: Gabriel Capella
// Date: 2015-10-13
//
////////////////////////////////////////////////////////////// */
#include <list>
#include <map>
#include <set>
#include <string>
#include <vector>

#include "firstandfollow.h"
#include "production.h"

#ifndef ITEM_H
#define ITEM_H

using namespace std;

/* Production with dot and lookahead */
class Item {
   public:
    Production *production;
    long unsigned point;
    long unsigned lookahead;

    Item(Production *production);
    ~Item();
    bool operator<(const Item &a) const;
    bool operator==(const Item &a) const;
    string str(map<int, string> &id_name);
    int operator[](const int n);
    long unsigned from();
};

class ItemsSet : public set<Item> {
   public:
    int id;
    ItemsSet();
    ~ItemsSet(){};
    void closure(Productions *productions, FirstFollow &ff);
    ItemsSet Goto(int X, Productions *productions, FirstFollow &ff);
    string str(map<int, string> &id_name);
};

#endif
