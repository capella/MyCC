////////////////////////////////////////////////////////////// */
//
// Author: Gabriel Capella
// Date: 2015-10-13
//
////////////////////////////////////////////////////////////// */
#include "production.h"

// ----------------------- PRODUCTION -----------------------
Production::Production(int from) : from(from) { vector<int>(); }

Production::~Production() {}

string Production::str(map<int, string> &id_name) {
    unsigned int i;
    string s = "" + id_name[from] + " -> ";
    for (i = 0; i < this->size(); ++i) {
        unsigned int id = (*this)[i];
        s += id_name[id] + " ";
    }
    return s;
}
