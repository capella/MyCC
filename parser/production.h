////////////////////////////////////////////////////////////// */
//
// Author: Gabriel Capella
// Date: 2015-10-13
//
////////////////////////////////////////////////////////////// */
#include <list>
#include <map>
#include <set>
#include <string>
#include <vector>

#ifndef PRODUCTION_H
#define PRODUCTION_H

using namespace std;

/* A -> aBBCab
   A is from
   aBBCab is a list of terms
   */
class Production : public vector<int> {
   public:
    Production(int from);
    ~Production();
    unsigned int from;
    string str(map<int, string> &id_name);
};

class Productions : public map<int, vector<Production *> > {
   public:
    int size();
};

#endif
