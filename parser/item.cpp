////////////////////////////////////////////////////////////// */
//
// Author: Gabriel Capella
// Date: 2015-10-13
//
////////////////////////////////////////////////////////////// */
#include "item.h"

// ----------------------- ITEM -----------------------
Item::Item(Production *production) : production(production) {
    point = 0;
    lookahead = -1;
}

bool Item::operator<(const Item &a) const {
    if (!(*production == *(a.production)))
        return *production < *(a.production);
    else if (point != a.point)
        return point < a.point;
    else
        return lookahead < a.lookahead;
}

bool Item::operator==(const Item &a) const {
    bool tmp = *production == *(a.production);
    tmp = tmp && (point == a.point);
    tmp = tmp && (lookahead == a.lookahead);
    return tmp;
}

string Item::str(map<int, string> &id_name) {
    unsigned int i;
    string s = "" + id_name[production->from] + " -> ";
    for (i = 0; i < production->size(); ++i) {
        if (i == point) s += ". ";
        unsigned int id = (*production)[i];
        if (id_name[id].size() == 0)
            s += "$ ";
        else
            s += id_name[id] + " ";
    }
    if (i == point) s += ". ";
    s += "\t(" + id_name[lookahead] + ")";
    return s;
}

int Item::operator[](int n) { return (*production)[n]; }

Item::~Item() {}

// ----------------------- ITEMSSET -----------------------
ItemsSet::ItemsSet() { id = 0; }

void ItemsSet::closure(Productions *productions, FirstFollow &ff) {
    int changes = 1;
    while (changes) {
        changes = 0;

        // for 􏰍 each item A 􏰆-> 􏰂􏰭a.Bb􏰃 in J 􏰎
        ItemsSet::iterator A;
        for (A = begin(); A != end(); ++A) {
            Item a = *A;

            if (a.point >= a.production->size()) continue;
            int B = a.production->at(a.point);

            // Create Ba
            vector<int> Ba;
            for (unsigned int j = a.point + 1;
                 j < a.production->size(); ++j)
                Ba.push_back(a.production->at(j));
            Ba.push_back(a.lookahead);

            // for 􏰍 each production B 􏰆-> l of G 􏰎
            for (unsigned int i = 0; i < (*productions)[B].size();
                 i++) {
                // for 􏰍 each terminal b in FIRST􏰍􏰃 (Ba)
                vector<int> first = ff.firststring(Ba);
                for (unsigned int j = 0; j < first.size(); ++j) {
                    if (first[j] >= ff.firstnoterminal) continue;

                    Production *prod = (*productions)[B][i];
                    Item item(prod);
                    item.lookahead = first[j];

                    pair<ItemsSet::iterator, bool> ret = insert(item);
                    if (ret.second) changes++;
                }
            }
        }
    }
}

string ItemsSet::str(map<int, string> &id_name) {
    string s = "-------------\n";
    ItemsSet::iterator A;
    for (A = begin(); A != end(); ++A) {
        Item a = *A;
        s += a.str(id_name) + "\n";
    }
    s += "-------------\n";
    return s;
}

ItemsSet ItemsSet::Goto(int X, Productions *productions,
                        FirstFollow &ff) {
    ItemsSet J;
    ItemsSet::iterator Ap;
    // for any item A->a.Xb
    for (Ap = begin(); Ap != end(); ++Ap) {
        Item A = *Ap;
        if (A.point == A.production->size()) continue;
        if (A[A.point] != X) continue;
        // Item newitem = *A;
        A.point++;
        if (A.production->size() < A.point) continue;
        J.insert(A);
    }
    J.closure(productions, ff);
    return J;
}
