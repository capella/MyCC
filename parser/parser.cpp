#include <stdlib.h>

#include <algorithm>
#include <cctype>
#include <cstring>
#include <fstream>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#include "firstandfollow.h"
#include "item.h"
#include "lex.h"

using namespace std;

Productions *read_file(char const *file, map<int, string> &id_name,
                       map<string, int> &name_id, int &firstnot) {
    ifstream t(file);
    stringstream buffer;
    buffer << t.rdbuf();

    Lex lex;
    string whiteStr = "WHITE";
    lex.parse(buffer.str());
    lex.removeToken(whiteStr);
    firstnot = 0;  // first no temrinal

    unsigned int i;
    for (i = 0; i < lex.size(); ++i) {
        // cout << lex.name(lex[i]) << "\n";
        if (lex.name(lex[i]) == "TERMINAL" &&
            name_id.find(lex.text(i)) == name_id.end()) {
            name_id[lex.text(i)] = firstnot;
            id_name[firstnot] = lex.text(i);
            firstnot++;
        }
    }

    name_id["$"] = firstnot;
    id_name[firstnot] = "$";
    firstnot++;

    // name_id["?"] = firstnot;
    // id_name[firstnot] = "?";
    // firstnot++;

    int k = firstnot;
    for (i = 0; i < lex.size(); ++i) {
        if (lex.name(lex[i]) == "TOKEN" &&
            name_id.find(lex.text(i)) == name_id.end()) {
            name_id[lex.text(i)] = k;
            id_name[k] = lex.text(i);
            k++;
        }
    }

    name_id["@"] = id_name.size();
    id_name[id_name.size()] = "@";

    Productions *productions = new Productions();
    Production *production = NULL;

    int id = -1;
    for (i = 0; i < lex.size(); ++i) {
        string name = lex.name(lex[i]);
        // cout << name << "\t'" << lex.text(i) << "'" << "\n";
        if (name == "NEW" && i + 1 < lex.size()) {
            i++;
            if (production != NULL)
                (*productions)[id].push_back(production);
            production = NULL;
            id = name_id[lex.text(i)];
        } else if (name == "POINT" && id != -1) {
            if (production != NULL)
                (*productions)[id].push_back(production);
            production = new Production(id);
            production->from = id;
        } else if ((name == "TOKEN" || name == "TERMINAL") &&
                   production != NULL) {
            production->push_back(name_id[lex.text(i)]);
        } else {
            cout << "ERROR parsing.\n";
        }
    }

    if (production != NULL) (*productions)[id].push_back(production);

    // DEBUG
    // Productions::iterator it;
    // for (it = productions->begin(); it != productions->end(); it++)
    // {
    //     vector <Production*> prods = it->second;
    //     for (unsigned int i = 0; i < prods.size(); i++) {
    //         cout << id_name[it->first] << "\t" <<
    //         prods.at(i)->str(id_name) << "\n";
    //     }
    // }

    return productions;
}

void print_int_vector(vector<int> v, string name) {
    cout << "int " << name << "[] = {";
    for (unsigned int i = 0; i < v.size(); ++i) {
        cout << v[i];
        if (i != v.size() - 1) cout << ", ";
        if (i % 30 == 29) cout << "\n";
    }
    cout << "};\n";
}

void print_table(int **A, int lins, int cols) {
    cout << "int ACTION[][" << cols << "] = {\n";
    for (int i = 0; i < lins; ++i) {
        cout << "   {";
        for (int j = 0; j < cols; ++j) {
            cout << A[i][j];
            if (j != cols - 1) cout << ", ";
        }
        cout << "}";
        if (i != lins - 1) cout << ", ";
        cout << "\n";
    }
    cout << "};\n";
}

bool cmp(pair<string, int> &a, pair<string, int> &b) {
    return a.second < b.second;
}

int main(int argc, char const *argv[]) {
    if (argc < 2) {
        cout << "usage: parser [-h] <par_file>\n\n";
        cout << " -h\t\t\tprint headers (.h) file\n";
        return EXIT_SUCCESS;
    }
    int firstnot;

    map<int, string> id_name;
    map<string, int> name_id;
    Productions *productions =
        read_file(argv[argc - 1], id_name, name_id, firstnot);

    // make initial ruler
    // @ -> $
    Production initial(name_id["@"]);
    initial.push_back(firstnot);
    // initial.push_back(name_id["?"]);
    (*productions)[name_id["@"]].push_back(&initial);

    // Create initial item
    Item fitem(&initial);
    fitem.lookahead = name_id["$"];

    // Create initial set
    ItemsSet initialset;
    initialset.insert(fitem);

    // Create a set of ItemsSet
    set<ItemsSet> T;
    T.insert(initialset);

    // Calc First and follow
    FirstFollow ff(name_id, firstnot, productions);

    // Init set of itens set
    set<ItemsSet> C;
    initialset.closure(productions, ff);
    C.insert(initialset);

    // for 􏰍 each set of items I in C
    int changes = 1;
    set<ItemsSet>::iterator I;
    while (changes) {
        changes = 0;
        for (I = C.begin(); I != C.end(); I++) {
            for (unsigned int X = 0; X < name_id.size(); ++X) {
                ItemsSet s = *I;
                s = s.Goto(X, productions, ff);
                if (s.size() == 0) continue;

                pair<set<ItemsSet>::iterator, bool> ret = C.insert(s);
                if (ret.second) changes++;
            }
        }
    }

    // Debug
    // cout << "/*\n";
    // for (I = C.begin(); I != C.end(); I++) {
    //     ItemsSet s = *I;
    //     cout << s.str(id_name);
    // }
    // cout << "*/\n";

    // Create a list of sets
    vector<ItemsSet> list;
    int count = 0;
    for (I = C.begin(); I != C.end(); I++) {
        ItemsSet s = *I;
        s.id = count;
        list.push_back(s);
        count++;
    }

    // in A
    // A[i][j] >= 0, shift j
    // A[i][j] = -1, error
    // A[i][j] = -2, accept
    // A[i][j] < -2, reduce to -A[i][j]-3
    int **A;
    A = (int **)malloc(sizeof(int *) * list.size());
    for (unsigned int i = 0; i < list.size(); ++i) {
        A[i] = (int *)malloc(sizeof(int) * (name_id.size()));
        for (unsigned int j = 0; j < name_id.size(); ++j)
            A[i][j] = -1;  // ERROR
    }

    // reduce info
    vector<int> reduce_n;
    vector<int> reduce_to;

    // page 265 of the dragon - ruler B
    for (unsigned int i = 0; i < list.size(); ++i) {
        ItemsSet::iterator it;
        // RULER a)
        for (it = list[i].begin(); it != list[i].end(); it++) {
            Item item = *it;
            if (item.point >= item.production->size()) continue;
            int a = item.production->at(item.point);
            if (a >= firstnot) continue;
            ItemsSet tmp = list[i].Goto(a, productions, ff);

            // see all sets and find the equal one
            for (unsigned int j = 0; j < list.size(); ++j) {
                if (tmp != list[j]) continue;
                if (A[i][a] != (int)j && A[i][a] != -1) {
                    cout << "Error 4 " << A[i][a] << "\n";
                    return EXIT_FAILURE;
                }
                A[i][a] = j;
            }
        }

        // RULER b)
        for (it = list[i].begin(); it != list[i].end(); it++) {
            Item item = *it;
            if (item.point != item.production->size()) continue;
            if ((int)item.production->from == name_id["@"]) continue;
            int a = item.lookahead;

            if (A[i][a] != -1) {
                // cout << "Error 1 " << A[i][a] << "\n";
                continue;
            }

            reduce_to.push_back(item.production->from);
            reduce_n.push_back(item.point);
            A[i][a] = -2 - reduce_to.size();
        }

        // RULER c)
        for (it = list[i].begin(); it != list[i].end(); it++) {
            Item item = *it;
            if (item.production != &initial) continue;
            if (item.point != item.production->size()) continue;
            int a = name_id["$"];
            if (A[i][a] != -2 && A[i][a] != -1) {
                cout << "Error 4 " << A[i][a] << "\n";
                return EXIT_FAILURE;
            }
            A[i][a] = -2;
        }
    }

    // STEP 3
    for (unsigned int i = 0; i < list.size(); ++i) {
        ItemsSet::iterator it;
        for (unsigned int a = firstnot; a < id_name.size(); ++a) {
            ItemsSet tmp = list[i].Goto(a, productions, ff);

            // see all sets and find the equal one
            for (unsigned int j = 0; j < list.size(); ++j) {
                if (tmp != list[j]) continue;
                if (A[i][a] != (int)j && A[i][a] != -1) {
                    cout << "Error " << A[i][a] << "\n";
                    return EXIT_FAILURE;
                }
                A[i][a] = j;
            }
        }
    }

    // STEP 5
    int initial_state = -1;
    for (unsigned int i = 0; i < list.size(); ++i) {
        ItemsSet::iterator it;
        // RULER a)
        for (it = list[i].begin(); it != list[i].end(); it++) {
            Item item = *it;
            if (item.production != &initial) continue;
            if (item.point != 0) continue;
            if ((int)item.lookahead != name_id["$"]) continue;
            initial_state = i;
        }
    }

    vector<pair<string, int>> sorted;
    for (pair<string, int> i : name_id) sorted.push_back(i);
    sort(sorted.begin(), sorted.end(), cmp);
    if (strcmp(argv[1], "-h") == 0) {
        cout << "#ifndef PARSER_P_H\n"
             << "#define PARSER_P_H\n\n"
             << "namespace P {\n"
             << "    enum Token {\n";
        for (pair<string, int> it : sorted) {
            string display = it.first;
            display[0] = toupper(display[0]);
            if (display.compare("@") == 0) display = "__START";
            if (display.compare("$") == 0) display = "__END";

            cout << "       " << display << " = " << it.second
                 << ",\n";
        }
        cout << "    };\n";

        cout << "    const char TOKEN_TO_STRING[" << sorted.size()
             << "][32] = {\n";
        for (pair<string, int> it : sorted) {
            string display = it.first;
            display[0] = toupper(display[0]);
            if (display.compare("@") == 0) display = "__START";
            if (display.compare("$") == 0) display = "__END";

            cout << "       \"" << display << "\",\n";
        }
        cout << "    };\n\n";

         cout    << "}\n\n#endif\n";
    } else {
        cout << "int initruler = " << initial_state << ";\n";
        cout << "int firstnot = " << firstnot << ";\n";

        print_int_vector(reduce_to, "reduceto");
        print_int_vector(reduce_n, "reduceN");
        print_table(A, list.size(), name_id.size());


        cout << "Parser::Parser(){\n";
        cout << "   erroposition = 0;\n";
        for (pair<string, int> it : sorted) {
            cout << "   tokens[\"" << it.first
                 << "\"] = " << it.second << ";\n";
            cout << "   id_name[" << it.second << "] = \"" << it.first
                 << "\";\n";
        }
        cout << "}\n";

    }

    for (unsigned int i = 0; i < list.size(); ++i) {
        free(A[i]);
    }
    free(A);

    delete productions;

    return EXIT_SUCCESS;
}
