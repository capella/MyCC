////////////////////////////////////////////////////////////// */
//
// Author: Gabriel Capella
// Date: 2015-10-13
//
////////////////////////////////////////////////////////////// */
#include <iostream>

#include "parser.h"
// insert the data genereted by lex in next line

using namespace std;
/**DATA**/

// lextokens tokens from lex from 0 N
// ids parser ids (bseds in tokens from lex)
TreeNode *Parser::parse(vector<int> buffer,
                        vector<string> lextokens) {
    vector<int> v;
    vector<int> old = buffer;
    unsigned int i;

    TreeNode *n = NULL;

    for (i = 0; i < lextokens.size(); ++i) {
        if (this->tokens.find(lextokens[i]) != this->tokens.end()) {
            v.push_back(this->tokens[lextokens[i]]);
        } else {
            v.push_back(-1);
        }
    }
    for (i = 0; i < buffer.size(); ++i) {
        if (v[buffer[i]] == -1) {
            error =
                "token " + lextokens[buffer[i]] + " not in parser!\n";
            erroposition = -1;
            return NULL;
        }
        // cout << " " << buffer[i];
        buffer[i] = v[buffer[i]];
    }

    // add eof
    buffer.push_back(tokens["$"]);
    stack<TreeNode *> nodestack;
    stack<int> stack;
    stack.push(initruler);

    int index = 0;
    int a = buffer[index];
    while (1) {
        int s = stack.top();
        if (ACTION[s][a] >= 0) {
            stack.push(ACTION[s][a]);
            nodestack.push(
                new TreeNode(index, static_cast<P::Token>(a), NULL));
            index++;
            a = buffer[index];
        } else if (ACTION[s][a] < -2) {
            int p = -ACTION[s][a] - 3;
            n = new TreeNode(-1, static_cast<P::Token>(reduceto[p]),
                             NULL);
            for (int j = 0; j < reduceN[p]; ++j) {
                stack.pop();
                n->add(nodestack.top());
                nodestack.pop();
            }
            int t = stack.top();
            int A = reduceto[p];
            stack.push(ACTION[t][A]);

            // OUTPUT
            nodestack.push(n);
        } else if (ACTION[s][a] == -2) {
            // accept
            return nodestack.top();
        } else {
            // ERROR RECOVERY
            break;
        }

        error = "not expecting this token.";
        erroposition = index;
    }
    return NULL;
}

