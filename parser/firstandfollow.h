////////////////////////////////////////////////////////////// */
//
// Author: Gabriel Capella
// Date: 2015-10-13
//
////////////////////////////////////////////////////////////// */
#include <list>
#include <map>
#include <set>
#include <string>
#include <vector>

#include "production.h"

#ifndef FF_H
#define FF_H

class FirstFollow {
   private:
    bool allnuable(Production &tokens);
    bool allnuable(int i, int j, Production &tokens);
    set<int> firststring(int c, vector<int> &s);

   public:
    int firstnoterminal;
    vector<bool> nuable;
    vector<set<int> > first;
    vector<set<int> > follow;
    FirstFollow(map<string, int> &name_id, int firstnot,
                Productions *productions);
    vector<int> firststring(vector<int> s);
};

#endif
