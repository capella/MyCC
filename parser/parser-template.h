////////////////////////////////////////////////////////////// */
//
// Author: Gabriel Capella
// Date: 2015-10-13
//
////////////////////////////////////////////////////////////// */
#include <list>
#include <map>
#include <stack>
#include <string>
#include <vector>

#ifndef PARSER_H
#define PARSER_H

#include "treenode.h"

using namespace std;

class Parser {
    map<string, int> tokens;
    map<int, string> id_name;

   public:
    Parser();
    TreeNode* parse(vector<int> ids, vector<string> lextokens);
    int erroposition;
    string error;
};

#endif
