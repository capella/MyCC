////////////////////////////////////////////////////////////// */
//
// Author: Gabriel Capella
// Date: 2015-10-13
//
////////////////////////////////////////////////////////////// */
#include <string>
#include <utility>
#include <vector>

#ifndef LEX_H
#define LEX_H

using namespace std;

class Lex {
   private:
    vector<string> names;            // avaliables names
    vector<string> data;             // text values of token
    vector<int> ids;                 // token ids
    vector<pair<int, int> > iniend;  // token ids
   public:
    Lex();

    // receive a string. Return 1 if error 0 if not
    // the string contains the error description
    bool parse(string str);

    // givem one token id, get it name
    string name(int id);

    // givem one name id, get it token
    int id(string name);

    // list all avaliable names
    vector<string> namesList();

    // list all avaliable names
    void removeToken(string &name);

    // return all tokens
    vector<int> tokens();

    // given one vector id, token id
    int operator[](unsigned int p);

    // size of tokens
    unsigned int size();

    // return the data of position
    string text(unsigned int i);

    // get token position and return p in str
    int begin(unsigned int i);
    int end(unsigned int i);

    int erroposition;
    string error;
};

#endif
