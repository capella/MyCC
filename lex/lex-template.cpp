////////////////////////////////////////////////////////////// */
//
// Author: Gabriel Capella
// Date: 2015-10-13
//
////////////////////////////////////////////////////////////// */
#include "lex.h"
// insert the data genereted by lex in next line

using namespace std;
/**DATA**/

bool Lex::parse(string str) {
    int lf = 0;
    int cs = 1;

    int dfap = 0;    // automato position
    int finalp = 0;  // last p. with regnized with final state
    int start = 0;

    bool status = 0;  // used to return error

    while (1) {
        // printf("%d %d \t %d '%c' \t %d  %d \t\n", lf, cs, dfap,
        // str[dfap], start, finalp);
        cs = edges[cs][(int)str[dfap]];
        dfap++;
        if (final[cs] != -1) {
            lf = cs;
            finalp = dfap;
        }

        if (cs == 0) {
            if ((this->names)[final[lf]] == "ERROR") {
                ids.clear();
                data.clear();
                iniend.clear();

                error = "invalid charecter '" + str.substr(start, 1) +
                        "'.";
                erroposition = start;
                status = 1;
                break;
            }
            // printf("-----------------------\n");
            data.push_back(str.substr(start, finalp - start));
            iniend.push_back(pair<int, int>(start, finalp));
            ids.push_back(final[lf]);

            cs = 1;
            lf = 0;
            start = finalp;
            dfap = start;
            if (str[dfap] == '\0') break;
        }
    }
    return status;
}

string Lex::name(int id) { return names[id]; }

int Lex::id(string name) {
    unsigned int i;
    for (i = 0; i < this->names.size(); ++i) {
        if (name.compare(this->names[i]) == 0) return i;
    }
    return -1;
}

vector<string> Lex::namesList() { return names; }

void Lex::removeToken(string &name) {
    unsigned int i;
    unsigned int count = 0;

    for (i = 0; i < this->size(); ++i) {
        if (this->name(ids[i]) != name) {
            ids[count] = ids[i];
            data[count] = data[i];
            iniend[count] = iniend[i];
            count++;
        }
    }
    ids.resize(count);
    data.resize(count);
    iniend.resize(count);
}

vector<int> Lex::tokens() { return ids; }

int Lex::operator[](unsigned int p) { return ids[p]; }

unsigned int Lex::size() { return ids.size(); }

string Lex::text(unsigned int i) { return data[i]; }

int Lex::begin(unsigned int p) { return iniend[p].first; }

int Lex::end(unsigned int p) { return iniend[p].second; }
