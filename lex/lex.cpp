#include <stdlib.h>

#include <algorithm>
#include <fstream>
#include <iostream>
#include <list>
#include <set>
#include <stack>
#include <vector>
#define ERROR 0
#define SUCESS 1

using namespace std;

class NFANode {
   public:
    vector<string> strings;
    vector<NFANode *> nodes;
    vector<int> transitions;
    int accepeted;
    int visited;
    NFANode() {
        this->accepeted = -1;
        visited = 0;
    };
    int makeRealTransition() {
        int error = SUCESS;
        if (this->transitions.size() != 0) return SUCESS;
        NFANode *add_error = NULL;

        for (unsigned int k = 0; k < this->nodes.size(); ++k) {
            if (this->strings[k].size() == 2) {
                char c = this->strings[k][1];
                if (c == 'n') c = '\n';
                if (c == 't') c = '\t';
                if (c == 's') c = ' ';
                if (c == 'r') c = '\r';
                // ERROR
                if (c == 'A') add_error = this->nodes[k];
                this->transitions.push_back(c);

            } else if (this->strings[k].size() == 1)
                this->transitions.push_back(this->strings[k][0]);
            else if (this->strings[k].size() == 0)
                this->transitions.push_back(-1);
            else {
                cout << "Error transitions! " << this->strings[k]
                     << "\n";
                return ERROR;
            }
            error &= this->nodes[k]->makeRealTransition();
        }
        if (add_error) {
            for (int i = 0; i < 256; ++i) {
                this->transitions.push_back(i);
                this->nodes.push_back(add_error);
                string s = "1";
                s[0] = (char)i;
                this->strings.push_back(s);
            }
        }
        return error;
    };

    void print() {
        if (this->visited == 1) return;
        this->visited = 1;
        if (this->accepeted != -1)
            cout << "node [shape = doublecircle]; "
                 << (long unsigned int)this << "; \n";

        for (unsigned int k = 0; k < this->nodes.size(); ++k) {
            cout << (long unsigned int)this << " -> "
                 << (long unsigned int)this->nodes[k];
            string s = this->strings[k];
            if (s.size() >= 1) {
                s = "\\" + s;
            }
            cout << " [ label = \"" << s << "\" ];\n";
            this->nodes[k]->print();
        }
    }
};

set<NFANode *> edges(NFANode *s, int trasition) {
    set<NFANode *> t;
    for (unsigned int i = 0; i < s->transitions.size(); i++) {
        if (trasition == s->transitions[i]) {
            t.insert(s->nodes[i]);
        }
    }
    return t;
}

set<NFANode *> closure(set<NFANode *> S) {
    set<NFANode *> t;
    set<NFANode *> tt;
    set<NFANode *>::iterator s;
    t = S;
    while (t != tt) {
        tt = t;
        for (s = tt.begin(); s != tt.end(); ++s) {
            set<NFANode *> e = edges(*s, -1);
            t.insert(e.begin(), e.end());
        }
    }
    return t;
}

set<NFANode *> DFAedge(set<NFANode *> d, int c) {
    set<NFANode *> t;
    set<NFANode *>::iterator s;

    for (s = d.begin(); s != d.end(); ++s) {
        set<NFANode *> e = edges(*s, c);
        t.insert(e.begin(), e.end());
    }
    return closure(t);
}

int subs(string *exp, vector<string> names, vector<string> exps) {
    long unsigned int p = 0, i;
    int count;
    int subs_total = 0;
    while ((p = exp->find_first_of('\\', p)) != string::npos) {
        long unsigned int k = p;
        while (k + 1 < exp->size() && (*exp)[k + 1] <= 'Z' &&
               (*exp)[k + 1] >= 'A') {
            // cout << exp[k+1] << " "<< k+1 << " "<< p  <<"\n";
            k++;
        }
        if (k != p) {
            string token = exp->substr(p + 1, k - p);
            count = 0;
            for (i = 0; i < names.size(); ++i) {
                if (names[i] == token) {
                    string n = "(" + exps[i] + ")";
                    exp->replace(p, k - p + 1, n);
                    count++;
                    subs_total++;
                }
            }
            if (!count) {
                cout << "Token " << token << " not found!\n";
                return -1;
            }
        }
        p = p + 1;
    }
    return subs_total;
}

void clean_expression(string *s);
NFANode *makeNFA(string s, int id);
void buildNFA(NFANode *begin);

int main(int argc, char const *argv[]) {
    if (argc < 2) {
        printf("Enter: lex <grammer_file>\n");
        return EXIT_SUCCESS;
    }

    char const *lexfilename = argv[1];
    // char const *outputdirname = argv[2];

    ifstream lexfile(lexfilename);

    if (!lexfile.is_open()) {
        cout << "grammer_file don't exist.\n";
        return EXIT_SUCCESS;
    }

    string line;
    vector<NFANode *> tokens;
    vector<string> names;
    vector<string> expression;
    int line_count = 1;

    while (getline(lexfile, line)) {
        if (line == "") continue;
        if (line.size() >= 2 && line[0] == '/' && line[1] == '/')
            continue;
        int endtoken = line.find_first_of(":");
        int endline = line.size();

        if (endtoken != -1 && endtoken + 1 < endline) {
            names.push_back(line.substr(0, endtoken));
            expression.push_back(line.substr(endtoken + 1, endline));
        } else {
            cout << "Error line: " << line_count << "\n";
            return EXIT_SUCCESS;
        }
        line_count++;
    }
    lexfile.close();

    NFANode begin;

    for (unsigned int i = 0; i < names.size(); ++i) {
        clean_expression(&expression[i]);
    }

    int subs_count = 0;
    int c = 1;
    unsigned int jj;
    while (c && subs_count < 20) {
        c = 0;
        for (jj = 0; jj < names.size(); ++jj) {
            int subscount = subs(&expression[jj], names, expression);
            if (subscount == -1) return EXIT_SUCCESS;
            c += subscount;
        }
        subs_count++;
    }

    // ERROR
    expression.push_back("\\A");
    names.push_back("ERROR");
    for (jj = 0; jj < names.size(); ++jj) {
        tokens.push_back(makeNFA(expression[jj], jj));
        // cout << "> "<< names[jj] << "\n";
        // cout << expression[jj] << "\n";
    }

    for (unsigned int i = 0; i < tokens.size(); ++i) {
        begin.strings.push_back("");
        begin.nodes.push_back(tokens[i]);
    }

    if (begin.makeRealTransition() == ERROR) {
        return EXIT_SUCCESS;
    }
    // begin.print();

    // make DFA
    set<NFANode *> empy;
    set<NFANode *> s1;
    s1.insert(&begin);
    vector<set<NFANode *> > states;
    states.push_back(empy);
    states.push_back(closure(s1));
    vector<vector<int> > tran;

    int p = 1;
    int j = 0;
    int i;

    while (j <= p) {
        vector<int> line;
        line.resize(256);
        tran.push_back(line);

        for (int c = 0; c < 256; c++) {
            set<NFANode *> e = DFAedge(states[j], c);
            // if e = states[i] for some i <= p
            for (i = 0; i <= p; i++) {
                if (e == states[i]) break;
            }
            if (i <= p) {
                tran[j][c] = i;
            } else {
                p++;
                // states[p] = e;
                states.push_back(e);
                tran[j][c] = p;
            }
        }
        j++;
    }

    if (argc > 2) return EXIT_SUCCESS;

    printf("int edges[][256] = {\n");
    for (unsigned int i = 0; i < tran.size(); i++) {
        printf("   /* STATE %4u */ {", i);
        for (unsigned int j = 0; j < tran[i].size(); j++) {
            if (j != 0) cout << ",";
            cout << tran[i][j];
        }
        cout << "}";
        if (i + 1 != tran.size()) cout << ",";
        cout << "\n";
    }
    printf("};\n");

    // find final stades
    printf("int final[] = {\n");
    for (unsigned int i = 0; i < states.size(); i++) {
        // cout << "S " << i <<"\n";
        int min = -2;
        set<NFANode *>::iterator s;
        for (s = states[i].begin(); s != states[i].end(); ++s) {
            if ((*s)->accepeted != -1 &&
                ((*s)->accepeted < min || min == -2))
                min = (*s)->accepeted;
        }
        min = min == -2 ? -1 : min;
        if (min != -1)
            cout << "   " << min << ", /* " << names[min] << " - "
                 << i << " */\n";
        else
            cout << "   " << min << ",\n";
    }
    printf("};\n");

    printf("Lex::Lex() {\n");
    cout << "   erroposition = 0;\n";
    for (unsigned int i = 0; i < names.size(); i++) {
        cout << "   names.push_back(\"" << names[i] << "\");\n";
    }
    cout << "   names.push_back(\"EOF\");\n";
    printf("}\n");

    return EXIT_SUCCESS;
}

void clean_expression(string *s) {
    s->erase(remove(s->begin(), s->end(), ' '), s->end());
}

NFANode *makeNFA(string s, int id) {
    NFANode *begin;
    NFANode *end;
    begin = new NFANode;
    end = new NFANode;
    begin->strings.push_back(s);
    begin->nodes.push_back(end);
    end->accepeted = id;
    buildNFA(begin);
    return begin;
}

void buildNFA(NFANode *begin) {
    int count_p = 0;
    int bar_flag = 0;
    unsigned int j;
    NFANode *tmp;
    for (unsigned int i = 0; i < begin->strings.size(); ++i) {
        string s = begin->strings[i];
        NFANode *end = begin->nodes[i];

        vector<int> values;
        // cout << s <<"\n";
        for (j = 0; j < s.size(); j++) {
            if (s[j] == '\\' && bar_flag == 0) {
                bar_flag = 1;
                values.push_back(count_p);
                continue;
            }
            bar_flag = 0;

            if (s[j] != '(' || (j != 0 && s[j - 1] == '\\')) {
                if (s[j] == ')' && j != 0 && s[j - 1] != '\\') {
                    count_p--;
                }
            } else {
                count_p++;
            }
            values.push_back(count_p);
        }

        int found = 0;
        for (j = 0; j < values.size(); j++) {
            // cout << values[j] <<"\n";
            if (values[j] == 0 && s[j] == '|' &&
                (j == 0 || s[j - 1] != '\\')) {
                string s1 = s.substr(0, j);
                string s2 = s.substr(j + 1, s.size() - j);

                tmp = new NFANode;
                tmp->nodes.push_back(end);
                tmp->strings.push_back("");
                begin->strings[i] = s1;
                begin->nodes[i] = tmp;

                tmp = new NFANode;
                begin->nodes[i] = tmp;
                tmp->nodes.push_back(end);
                tmp->strings.push_back("");
                begin->strings.push_back(s2);
                begin->nodes.push_back(tmp);

                i--;
                found = 1;
                // cout << s1 << "\t|\t"<< s2 <<"\n";
                break;
            }
        }
        if (found) continue;
        for (j = 0; j < values.size(); j++) {
            if (values[j] != 0) continue;
            if (s[j] == '\\') {
                j++;
            }
            string s1 = s.substr(0, j + 1);
            string s2 = s.substr(j + 1, s.size() - j);
            if (s2.size() <= 0) {
                if (s1[0] == '(' && s1[s1.size() - 1] == ')') {
                    s1 = s1.substr(1, s1.size() - 2);
                    begin->strings[i] = s1;
                    i--;
                }
                break;
            }

            if (s2[0] == '*') {
                s2 = s2.substr(1, s2.size() - 1);
                // cout << s1 << "\t*\t"<< s2 <<"\n";

                tmp = new NFANode;

                tmp->nodes.push_back(end);
                tmp->strings.push_back(s2);

                tmp->nodes.push_back(tmp);
                tmp->strings.push_back(s1);

                begin->strings[i] = "";
                begin->nodes[i] = tmp;

                buildNFA(tmp);
                break;
            }

            // cout << s1 << "\tX\t"<< s2 <<"\n";

            tmp = new NFANode;
            tmp->nodes.push_back(end);
            tmp->strings.push_back(s2);
            begin->strings[i] = s1;
            begin->nodes[i] = tmp;
            i--;
            buildNFA(tmp);
            break;
        }
    }
}
