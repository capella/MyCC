////////////////////////////////////////////////////////////// */
//
// Author: Gabriel Capella
// Date: 2015-10-13
//
////////////////////////////////////////////////////////////// */
#include <algorithm>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <stack>
#include <string>
#include <vector>

#ifndef GRAPH_H_
#define GRAPH_H_

using namespace std;

template <typename Type>
class Graph {
    map<Type, set<Type> > data;

   public:
    int degree(Type u);
    void add(Type u);
    void remove(Type u);
    void add_edge(Type u, Type v);
    void remove_edge(Type u, Type v);
    void print();
    int size();
    vector<Type> nodes();
    vector<Type> eddges(Type u);
};

template <typename Type>
class DiGraph {
    map<Type, set<Type> > to;
    map<Type, set<Type> > from;

   public:
    int degree_in(Type u);
    int degree_out(Type u);
    void add(Type u);
    void remove(Type u);
    void add_edge(Type u, Type v);
    void remove_edge(Type u, Type v);
    void print();
    int size();
    vector<Type> nodes();
    vector<Type> eddge_from(Type u);
    vector<Type> eddge_to(Type u);
};

#include "graph.hpp"

#endif