////////////////////////////////////////////////////////////// */
//
// Author: Gabriel Capella
// Date: 2015-10-13
//
////////////////////////////////////////////////////////////// */
#include <algorithm>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <stack>
#include <string>
#include <vector>

#ifndef INSTRUCTIONS_H
#define INSTRUCTIONS_H

#include "enviroment.h"
#include "error.h"
#include "graph.h"
#include "instruction.h"
#include "treenode.h"
#include "x86.h"

using namespace std;

#define MEM_POS_SIZE 8  // bytes 8 * 8 = 64
#define NUMBER_OF_REGISTERS 2

class Ids {
    int count;
    map<string, int> str_to_int;
    vector<int> sizes;

   public:
    Ids();
    ~Ids();
    // return the id number
    // -1 for BP
    int get(string s);
    void set_size(string s, int i);
    void set_size(int id, int i);
    int get_size(string s);
    int get_size(int i);
    int size();
    // return true has s
    bool has(string s);
};

void build_control_flow(TreeNode *n, Error *err, Ids &ids,
                        map<string, bool> &arg);

#endif
