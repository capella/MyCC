////////////////////////////////////////////////////////////// */
//
// Author: Gabriel Capella
// Date: 2015-10-13
//
////////////////////////////////////////////////////////////// */
#ifndef INSTRUCTION_H
#define INSTRUCTION_H

#include <iostream>
#include <string>
#include <vector>

#include "graph.h"

#if defined(__linux__)
#define PUTCHAR_NAME "putchar@PLT"
#define MALLOC_NAME "malloc@PLT"
#define MAIN_NAME "main"
#elif defined(__MACH__)
#define PUTCHAR_NAME "_putchar"
#define MALLOC_NAME "_malloc"
#define MAIN_NAME "_main"
#endif

using namespace std;

enum ops { MULT, DIV, MINUS, PLUS, LESS, AND, MOD };

class Instruction {
   public:
    // int size;
    virtual vector<int> def();
    virtual vector<int> use();
    virtual int jumps();
    virtual string name();
    virtual string asm_str(map<int, string> &m);
    virtual void change_use(int before, int after);
};

class Label : public Instruction {
   public:
    int url;
    string name();
    string asm_str(map<int, string> &m);
};

class Move : public Instruction {
   public:
    int to;
    vector<int> def();
    string name();
    string asm_str(map<int, string> &m);
};

class Print : public Instruction {
   public:
    int from;
    vector<int> use();
    void change_use(int before, int after);
    string name();
    string asm_str(map<int, string> &m);
};

class MovMalloc : public Move {
   public:
    int amount;
    string name();
    string asm_str(map<int, string> &m);
};

class MoveLiteral : public Move {
   public:
    int value;
    string name();
    string asm_str(map<int, string> &m);
};

class MenToReg : public Move {
   public:
    int from;  // from is an addr
    vector<int> use();
    void change_use(int before, int after);
    string name();
    string asm_str(map<int, string> &m);
};

class RegToMen : public Move {
   public:
    // to is one addr
    int from;
    vector<int> use();
    void change_use(int before, int after);
    vector<int> def();
    string name();
    string asm_str(map<int, string> &m);
};

class MoveMove : public Move {
   public:
    // move register to register
    int from;
    vector<int> use();
    void change_use(int before, int after);
    string name();
    string asm_str(map<int, string> &m);
};

class MoveNeg : public Move {
   public:
    // move register to register
    int from;
    vector<int> use();
    void change_use(int before, int after);
    string name();
    string asm_str(map<int, string> &m);
};

class MoveOp : public Move {
   public:
    int A;
    int operation;
    int B;
    vector<int> use();
    void change_use(int before, int after);
    string name();
    string asm_str(map<int, string> &m);
};

class JumpCond : public Instruction {
   public:
    int value;  // register id
    int to;     // label id
    // when false go to next code
    vector<int> use();
    void change_use(int before, int after);
    string name();
    string asm_str(map<int, string> &m);
};

class Jump : public Instruction {
   public:
    int to;  // label id
    int jumps();
    string name();
    string asm_str(map<int, string> &m);
};

class Pop : public Move {
   public:
    string name();
    string asm_str(map<int, string> &m);
};

class GetRet : public Move {
   public:
    string name();
    string asm_str(map<int, string> &m);
};

class PopDiscart : public Instruction {
   public:
    int size;  // number of bytes to discart
    string name();
    string asm_str(map<int, string> &m);
};

class SetRet : public Instruction {
   public:
    int from;
    vector<int> use();
    void change_use(int before, int after);
    string name();
    string asm_str(map<int, string> &m);
};

class Call : public Instruction {
   public:
    string to_name;
    int to_id;
    string name();
    string asm_str(map<int, string> &m);
};

class PushAll : public Instruction {
    string name();
    string asm_str(map<int, string> &m);
};

class PopAll : public Instruction {
    string name();
    string asm_str(map<int, string> &m);
};

class Push : public Instruction {
   public:
    int from;
    vector<int> use();
    void change_use(int before, int after);
    string name();
    string asm_str(map<int, string> &m);
};

class InstructionGraph : public DiGraph<Instruction *> {
   public:
    void print();
};

#endif
