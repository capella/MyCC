////////////////////////////////////////////////////////////// */
//
// Author: Gabriel Capella
// Date: 2015-10-13
//
////////////////////////////////////////////////////////////// */

#include "instruction.h"

vector<int> Instruction::def() {
    vector<int> v;
    return v;
}

int Instruction::jumps() { return -1; }

int Jump::jumps() { return to; }

vector<int> Instruction::use() {
    vector<int> v;
    return v;
}
void Instruction::change_use(int before, int after) {}

vector<int> Move::def() {
    vector<int> v;
    v.push_back(to);
    return v;
}

vector<int> Print::use() {
    vector<int> v;
    v.push_back(from);
    return v;
}

void Print::change_use(int before, int after) { from = after; }

vector<int> MenToReg::use() {
    vector<int> v;
    v.push_back(from);
    return v;
}

void MenToReg::change_use(int before, int after) { from = after; }

vector<int> RegToMen::use() {
    vector<int> v;
    v.push_back(from);
    v.push_back(to);
    return v;
}

void RegToMen::change_use(int before, int after) { from = after; }

vector<int> RegToMen::def() {
    vector<int> v;
    return v;
}

vector<int> MoveOp::use() {
    vector<int> v;
    v.push_back(A);
    v.push_back(B);
    return v;
}

void MoveOp::change_use(int before, int after) {
    if (A == before) A = after;
    if (B == before) B = after;
}

vector<int> MoveMove::use() {
    vector<int> v;
    v.push_back(from);
    return v;
}
void MoveMove::change_use(int before, int after) { from = after; }

vector<int> MoveNeg::use() {
    vector<int> v;
    v.push_back(from);
    return v;
}
void MoveNeg::change_use(int before, int after) { from = after; }

vector<int> JumpCond::use() {
    vector<int> v;
    v.push_back(value);
    return v;
}
void JumpCond::change_use(int before, int after) { value = after; }

vector<int> SetRet::use() {
    vector<int> v;
    v.push_back(from);
    return v;
}
void SetRet::change_use(int before, int after) { from = after; }

vector<int> Push::use() {
    vector<int> v;
    v.push_back(from);
    return v;
}
void Push::change_use(int before, int after) { from = after; }

string Instruction::name() { return "Instruction"; }

string Label::name() { return "Labe " + to_string(url); }

string Move::name() { return "Move"; }

string Print::name() { return "Prin %" + to_string(from); }

string MovMalloc::name() { return "MoMa " + to_string(to); }

string Push::name() { return "Push %" + to_string(from); }

string PushAll::name() { return "PuAl"; }
string PopAll::name() { return "PoAl"; }

string MoveLiteral::name() {
    return "MovL %" + to_string(to) + " <- " + to_string(value);
}
string MenToReg::name() {
    return "MToR %" + to_string(to) + " <- #" + to_string(from);
}

string RegToMen::name() {
    return "RToM #" + to_string(to) + " <- %" + to_string(from);
}

string MoveMove::name() {
    return "MoMo %" + to_string(to) + " <- %" + to_string(from);
}
string MoveNeg::name() { return "MNot"; }

string MoveOp::name() {
    return "MoOp %" + to_string(to) + " <- %" + to_string(A) + ",%" +
           to_string(B);
}
string JumpCond::name() { return "JuCo %" + to_string(value); }

string Jump::name() { return "Jump " + to_string(to); }

string Pop::name() { return "Pop  %" + to_string(to); }
string PopDiscart::name() { return "PopD"; }

string SetRet::name() { return "SRet"; }
// string Return::name() { return "Retu"; }
string GetRet::name() { return "GRet"; }

string Call::name() { return "Call (" + to_name + ")"; }

void InstructionGraph::print() {
    cout << "digraph {\n";
    for (auto j : nodes()) {
        cout << (unsigned long)j << " [ ";
        cout << "label = \"";
        cout << j->name();
        cout << "\" ];\n";

        cout << (unsigned long)j << "-> {";
        for (auto k : eddge_to(j)) cout << (unsigned long)k << " ";
        cout << "}; \n";
    }
    cout << "}";
}

string Instruction::asm_str(map<int, string> &m) {
    return "Instruction";
}

string Label::asm_str(map<int, string> &m) {
    return "\n__LABEL" + to_string(url) + ":\n";
}

string Move::asm_str(map<int, string> &m) { return "Move"; }

string Print::asm_str(map<int, string> &m) {
    string s = "";

    set<string> regs;
    stack<string> regs2;
    for (auto i : m)
        if (i.first != -1) regs.insert(i.second);

    for (auto i : regs) {
        s += "   pushq  " + i +
             "                   ## prepare print call\n";
        regs2.push(i);
    }

    s += "   pushq  " + m[from] + "                  ## print\n";

    s += "   movq   $1,%rax               ## sys_write\n";
    s += "   movq   $1,%rdi               ## fd stdout\n";
    s += "   movq   %rsp,%rsi             ## string pointer\n";
    s += "   movq   $1,%rdx               ## size\n";
    s += "   syscall                      ## syscall\n";
    s += "   popq   %rsi                  ##\n";

    while (!regs2.empty()) {
        s += "   popq   " + regs2.top() + "\n";
        regs2.pop();
    }

    return s;
}

string MovMalloc::asm_str(map<int, string> &m) {
    string s = "";

    set<string> regs;
    stack<string> regs2;
    for (auto i : m)
        if (i.first != -1) regs.insert(i.second);

    for (auto i : regs) {
        if (i == m[to]) continue;
        s += "   pushq  " + i +
             "                  ## prepare malloc\n";
        regs2.push(i);
    }

    s += "   movq    $" + to_string(amount) + ", %rdi\n";
    s += "   callq   " + string(MALLOC_NAME) + "\n";
    s += "   testq   %rax, %rax\n";
    s += "   jz      fail_exit\n";
    s += "   movq   %rax, " + m[to] + "\n";  // pop malloc value

    while (!regs2.empty()) {
        s += "   popq   " + regs2.top() + "\n";
        regs2.pop();
    }

    return s;
}

string Push::asm_str(map<int, string> &m) {
    return "   pushq  " + m[from] + "\n";
}

string PushAll::asm_str(map<int, string> &m) {
    string s = "";
    set<string> regs;
    stack<string> regs2;
    for (auto i : m)
        if (i.first != -1) regs.insert(i.second);
    for (auto i : regs) {
        s += "   pushq  " + i + "                  ## prepare call\n";
        regs2.push(i);
    }
    return s;
}

string PopAll::asm_str(map<int, string> &m) {
    string s = "";
    set<string> regs;
    stack<string> regs2;
    for (auto i : m)
        if (i.first != -1) regs.insert(i.second);

    for (auto i : regs) {
        regs2.push(i);
    }

    while (!regs2.empty()) {
        s += "   popq   " + regs2.top() + "\n";
        regs2.pop();
    }
    return s;
}

string MoveLiteral::asm_str(map<int, string> &m) {
    return "   movq   $" + to_string(value) + ", " + m[to] + "\n";
}
string MenToReg::asm_str(map<int, string> &m) {
    return "   movq   (" + m[from] + "), " + m[to] + "\n";
}

string RegToMen::asm_str(map<int, string> &m) {
    return "   movq   " + m[from] + ", (" + m[to] + ")\n";
}

string MoveMove::asm_str(map<int, string> &m) {
    if (m[from] != m[to])
        return "   movq   " + m[from] + ", " + m[to] + "\n";
    return "";
}

string MoveNeg::asm_str(map<int, string> &m) {
    return "   xor   $-1, " + m[to] + "\n";
}

string MoveOp::asm_str(map<int, string> &m) {
    string s = "";
    if (operation == AND) {
        if (m[A] == m[to]) {
            s += "   andq   " + m[B] + ", " + m[to] + "\n";
            s += "   andq   $1, " + m[to] + "\n";
        } else if (m[B] == m[to]) {
            s += "   andq   " + m[A] + ", " + m[to] + "\n";
            s += "   andq   $1, " + m[to] + "\n";
        } else {
            s += "   movq   " + m[A] + ", " + m[to] + "\n";
            s += "   andq   " + m[B] + ", " + m[to] + "\n";
            s += "   andq   $1, " + m[to] + "\n";
        }
        return s;
    }

    if (operation == PLUS) {
        if (m[A] == m[to]) {
            s += "   addq   " + m[B] + ", " + m[to] + "\n";
        } else if (m[B] == m[to]) {
            s += "   addq   " + m[A] + ", " + m[to] + "\n";
        } else {
            s += "   movq   " + m[A] + ", " + m[to] + "\n";
            s += "   addq   " + m[B] + ", " + m[to] + "\n";
        }
        return s;
    }

    if (operation == MINUS) {
        if (m[A] == m[to]) {
            s += "   subq   " + m[B] + ", " + m[to] + "\n";
        } else if (m[B] == m[to]) {
            s += "   imulq   $-1, " + m[to] + "\n";
            s += "   addq   " + m[A] + ", " + m[to] + "\n";
        } else {
            s += "   movq   " + m[A] + ", " + m[to] + "\n";
            s += "   subq   " + m[B] + ", " + m[to] + "\n";
        }
        return s;
    }
    if (operation == LESS) {
        string p = "%dil";
        s += "   cmpq   " + m[A] + ", " + m[B] + "\n";
        s += "   setle   " + p + "\n";
        s += "   xorb   $1, " + p + "\n";
        s += "   movzbq " + p + ", " + m[to] + "\n";
        return s;
    }
    if (operation == MULT) {
        if (m[A] == m[to]) {
            s += "   imul   " + m[B] + ", " + m[to] + "\n";
        } else if (m[B] == m[to]) {
            s += "   imul   " + m[A] + ", " + m[to] + "\n";
        } else {
            s += "   movq   " + m[A] + ", " + m[to] + "\n";
            s += "   imul   " + m[B] + ", " + m[to] + "\n";
        }
        return s;
    }
    if (operation == DIV) {
        if (m[A] == m[to]) {
            if (m[A] == "%rax") {
                s += "   pushq   %rdx\n";
                s += "   movq    " + m[B] + ", %rdi\n";
                s += "   cqto\n";
                s += "   idiv    %rdi\n";
                s += "   popq    %rdx\n";
            } else if (m[A] == "%rdx") {
                s += "   pushq   %rax\n";
                s += "   movq    %rdx, %rax\n";
                s += "   cqto\n";
                s += "   idiv    " + m[B] + "\n";
                s += "   movq    %rax, %rdx\n";
                s += "   popq    %rax\n";
            } else {
                s += "   pushq   %rax\n";
                s += "   pushq   %rdx\n";
                s += "   movq    " + m[B] + ", %rdi\n";
                s += "   movq    " + m[A] + ", %rax\n";
                s += "   cqto\n";
                s += "   idiv    %rdi\n";
                s += "   movq    %rax, " + m[A] + "\n";
                s += "   popq    %rdx\n";
                s += "   popq    %rax\n";
            }
        } else if (m[B] == m[to]) {
            if (m[B] == "%rax") {
                s += "   pushq   %rdx\n";
                s += "   movq    %rax, %rdi\n";
                s += "   movq    " + m[A] + ", %rax\n";
                s += "   cqto\n";
                s += "   idiv    %rdi\n";
                s += "   popq    %rdx\n";
            } else if (m[B] == "%rdx") {
                s += "   pushq   %rax\n";
                s += "   movq    %rdx, %rdi\n";
                s += "   movq    " + m[A] + ", %rax\n";
                s += "   cqto\n";
                s += "   idiv    %rdi\n";
                s += "   movq    %rax, %rdx\n";
                s += "   popq    %rax\n";
            } else {
                s += "   pushq   %rax\n";
                s += "   pushq   %rdx\n";
                s += "   movq    " + m[A] + ", %rax\n";
                s += "   cqto\n";
                s += "   idiv    " + m[B] + "\n";
                s += "   movq    %rax, " + m[B] + "\n";
                s += "   popq    %rdx\n";
                s += "   popq    %rax\n";
            }
        } else {
            if (m[to] != "%rax") s += "   pushq   %rax\n";
            if (m[to] != "%rdx") s += "   pushq   %rdx\n";
            s += "   movq    " + m[B] + ", %rdi\n";
            s += "   movq    " + m[A] + ", %rax\n";
            s += "   cqto\n";
            s += "   idiv    %rdi\n";
            s += "   movq    %rax, " + m[to] + "\n";
            if (m[to] != "%rdx") s += "   popq    %rdx\n";
            if (m[to] != "%rax") s += "   popq    %rax\n";
        }
        return s;
    }

    if (operation == MOD) {
        s += "#" + m[to] + " A:" + m[A] + " \t\t B:" + m[B] + "\n";
        // #%rdx A:%rdx          B:%rax
        if (m[A] == m[to]) {
            if (m[A] == "%rax") {
                s += "   pushq   %rdx\n";
                s += "   movq    " + m[B] + ", %rdi\n";
                s += "   cqto\n";
                s += "   idiv    %rdi\n";
                s += "   movq    %rdx, $rax\n";
                s += "   popq    %rdx\n";
            } else if (m[A] == "%rdx") {
                s += "   pushq   %rax\n";
                s += "   movq    " + m[B] + ", %rdi\n";
                s += "   movq    %rdx, %rax\n";
                s += "   cqto\n";
                s += "   idiv    %rdi\n";
                s += "   popq    %rax\n";
            } else {
                s += "   pushq   %rax\n";
                s += "   pushq   %rdx\n";
                s += "   movq    " + m[B] + ", %rdi\n";
                s += "   movq    " + m[A] + ", %rax\n";
                s += "   cqto\n";
                s += "   idiv    %rdi\n";
                s += "   movq    %rdx, " + m[A] + "\n";
                s += "   popq    %rdx\n";
                s += "   popq    %rax\n";
            }
        } else if (m[B] == m[to]) {
            if (m[B] == "%rax") {
                s += "   pushq   %rdx\n";
                s += "   movq    %rax, %rdi\n";
                s += "   movq    " + m[A] + ", %rax\n";
                s += "   cqto\n";
                s += "   idiv    %rdi\n";
                s += "   movq    %rdx, %rax\n";
                s += "   popq    %rdx\n";
            } else if (m[B] == "%rdx") {
                s += "   pushq   %rax\n";
                s += "   movq    %rdx, %rdi\n";
                s += "   movq    " + m[A] + ", %rax\n";
                s += "   cqto\n";
                s += "   idiv    %rdi\n";
                s += "   popq    %rax\n";
            } else {
                s += "   pushq   %rax\n";
                s += "   pushq   %rdx\n";
                s += "   movq    " + m[A] + ", %rax\n";
                s += "   cqto\n";
                s += "   idiv    " + m[B] + "\n";
                s += "   movq    %rdx, " + m[B] + "\n";
                s += "   popq    %rdx\n";
                s += "   popq    %rax\n";
            }
        } else {
            if (m[to] != "%rax") s += "   pushq   %rax\n";
            if (m[to] != "%rdx") s += "   pushq   %rdx\n";
            s += "   movq    " + m[B] + ", %rdi\n";
            s += "   movq    " + m[A] + ", %rax\n";
            s += "   cqto\n";
            s += "   idiv    %rdi\n";
            s += "   movq    %rdx, " + m[to] + "\n";
            if (m[to] != "%rdx") s += "   popq    %rdx\n";
            if (m[to] != "%rax") s += "   popq    %rax\n";
        }
        return s;
    }

    return "MoOp %" + to_string(to) + " <- %" + to_string(A) + ",%" +
           to_string(operation) + "\n";
}
string JumpCond::asm_str(map<int, string> &m) {
    string s = "";
    s += "   testq   " + m[value] + ", " + m[value] + "\n";
    s += "   jnz     __LABEL" + to_string(to) + "\n";

    return s;
}

string Jump::asm_str(map<int, string> &m) {
    return "   jmp    __LABEL" + to_string(to) + "\n";
}

string Pop::asm_str(map<int, string> &m) {
    return "   popq  " + m[to] + "\n";
}

string GetRet::asm_str(map<int, string> &m) {
    return "   movq  %rdi, " + m[to] + "\n";
}

string PopDiscart::asm_str(map<int, string> &m) {
    return "   add   $8, %rsp\n";
}

string SetRet::asm_str(map<int, string> &m) {
    string s = "";
    s += "   movq  " + m[from] + ", %rdi\n";
    s += "   movq    %rbp, %rsp \n";
    s += "   popq    %rbp\n";
    s += "   retq\n";
    return s;
}

string Call::asm_str(map<int, string> &m) {
    string s = "";
    s += "   call   __LABEL" + to_string(to_id) + "\n";
    return s;
}
