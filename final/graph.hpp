////////////////////////////////////////////////////////////// */
//
// Author: Gabriel Capella
// Date: 2015-10-13
//
////////////////////////////////////////////////////////////// */

#include "graph.h"

// ################### Graph ###################
template <typename Type>
vector<Type> Graph<Type>::nodes() {
    vector<Type> tmp;
    for (auto i : data) tmp.push_back(i.first);
    return tmp;
}

template <typename Type>
void Graph<Type>::add_edge(Type u, Type v) {
    if (u == v) return;
    data[u].insert(v);
    data[v].insert(u);
}

template <typename Type>
int Graph<Type>::degree(Type u) {
    if (data.count(u) == 0) return 0;
    return data[u].size();
}

template <typename Type>
int Graph<Type>::size() {
    return data.size();
}

template <typename Type>
void Graph<Type>::remove_edge(Type u, Type v) {
    if (data.count(u) != 0 && data[u].count(v)) data[u].erase(v);
    if (data.count(v) != 0 && data[u].count(u)) data[v].erase(u);
}

template <typename Type>
void Graph<Type>::add(Type u) {
    if (data.count(u) == 0) {
        set<Type> s;
        data[u] = s;
    }
}

template <typename Type>
void Graph<Type>::remove(Type u) {
    if (data.count(u) == 0) return;
    for (Type v : data[u]) {
        if (data.count(v) != 0) data[v].erase(u);
    }
    data.erase(u);
}

template <typename Type>
void Graph<Type>::print() {
    cout << "graph {\n";
    for (auto j : data) {
        cout << j.first << "-- {";
        for (auto k : j.second)
            if (k > j.first) cout << k << " ";
        cout << "}; \n";
    }
    cout << "}";
}

template <typename Type>
vector<Type> Graph<Type>::eddges(Type u) {
    vector<Type> tmp;
    for (auto i : data[u]) tmp.push_back(i);
    return tmp;
}

// ################### DIGRAPH ###################
template <typename Type>
vector<Type> DiGraph<Type>::nodes() {
    vector<Type> tmp;
    for (auto i : to) tmp.push_back(i.first);
    return tmp;
}

template <typename Type>
void DiGraph<Type>::add_edge(Type u, Type v) {
    if (u == v) return;
    to[u].insert(v);
    from[v].insert(u);
}

template <typename Type>
int DiGraph<Type>::degree_in(Type u) {
    if (to.count(u) == 0) return 0;
    return to[u].size();
}

template <typename Type>
int DiGraph<Type>::degree_out(Type u) {
    if (from.count(u) == 0) return 0;
    return from[u].size();
}

template <typename Type>
int DiGraph<Type>::size() {
    return to.size();
}

template <typename Type>
void DiGraph<Type>::remove_edge(Type u, Type v) {
    if (to.count(u) != 0 && to[u].count(v)) to[u].erase(v);
    if (from.count(v) != 0 && from[v].count(u)) from[v].erase(u);
}

template <typename Type>
void DiGraph<Type>::add(Type u) {
    if (to.count(u) == 0) {
        set<Type> s;
        to[u] = s;
        from[u] = s;
    }
}

template <typename Type>
void DiGraph<Type>::remove(Type u) {
    if (to.count(u) == 0) return;

    for (Type v : from[u]) {
        if (to.count(v) != 0) to[v].erase(u);
    }

    for (Type v : to[u]) {
        if (from.count(v) != 0) from[v].erase(u);
    }
    from.erase(u);
    to.erase(u);
}

template <typename Type>
void DiGraph<Type>::print() {
    cout << "digraph {\n";
    for (auto j : to) {
        cout << j.first << "-> {";
        for (auto k : j.second) cout << k << " ";
        cout << "}; \n";
    }
    cout << "}";
}

template <typename Type>
vector<Type> DiGraph<Type>::eddge_to(Type u) {
    vector<Type> tmp;
    for (auto i : to[u]) tmp.push_back(i);
    return tmp;
}

template <typename Type>
vector<Type> DiGraph<Type>::eddge_from(Type u) {
    vector<Type> tmp;
    for (auto i : from[u]) tmp.push_back(i);
    return tmp;
}
