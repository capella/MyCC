////////////////////////////////////////////////////////////// */
//
// Author: Gabriel Capella
// Date: 2015-10-13
//
////////////////////////////////////////////////////////////// */
#include "error.h"

void Error::print(int x) {
    int p = positions[x];
    string msg = msgs[x];

    int i = 0;
    int lastn = -1;
    int linecount = 1;
    int begin;
    int end;

    if (p == -1) {
        cerr << "ERRO: " << msg << "";
        return;
    }

    begin = lex->size() ? lex->begin(p) : p;
    end = lex->size() ? lex->end(p) : p;

    while (i <= begin) {
        if ((*code)[i] == '\n') {
            lastn = i;
            linecount++;
        }
        i++;
    }
    int e = code->find_first_of("\n", lastn + 1) - lastn - 1;
    string line = code->substr(lastn + 1, e);
    cerr << "ERRO in line " << linecount << ":\n";
    cerr << line;
    cerr << "\n";
    for (i = lastn + 1; i < begin; ++i) {
        if ((*code)[i] != '\t')
            cerr << " ";
        else
            cerr << "\t";
    }
    for (i = begin; i < end; ++i) cerr << "\033[1;31m^\033[0m";
    cerr << "\n";
    if (msg.size() > 0) {
        cerr << "Details: " << msg << "\n";
    }
}

Error::Error(Lex* lex, string* code) {
    this->code = code;
    this->lex = lex;
}

Error::~Error() {}

void Error::add(int position, string msg) {
    positions.push_back(position);
    msgs.push_back(msg);
}

int Error::size() { return msgs.size(); }

void Error::printall() {
    for (unsigned i = 0; i < msgs.size(); i++) {
        print(i);
        cerr << "\n";
    }
}
