#include <stdlib.h>

#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <vector>

#include "error.h"
#include "lex.h"
#include "parser.h"
#include "toinstructions.h"
#include "treenode.h"
// #include "syntaxtree.h"

using namespace std;

// get lex values and insert it contento to the node
void insert_content(TreeNode *n, Lex *lex);

// transfor all nodes in lists
void make_list(TreeNode *n, P::Token s);

void replace_type(TreeNode *n, P::Token from, P::Token to);

// set expressions to operation
void remove_expressions(TreeNode *n);
//  remove redutant call (one procedure next to iqual one)
TreeNode *simplify(TreeNode *node);
// remove unecessary leafs
TreeNode *remove_leafs(TreeNode *n);

void remove_nulls(TreeNode *n);
void remove_call_ids(TreeNode *n);

// create an eviroment each for each node
void creates_enviroments(TreeNode *n, Enviroment *env,
                         map<string, Class *> &classes);
// insert function in the enviroment
map<string, Class *> build_classes(TreeNode *n, Error *err);

// insert variable in the enviroment
void build_env_vars(TreeNode *n, Error *err,
                    map<string, Class *> &classes);

// verify if functions exist and the correct definitos
void verify_functions(TreeNode *n, Error *err);
// verify variables exist and the correct definitos
void verify_vars(TreeNode *n, Error *err);

// inside a calllist must have only ids!!!
void set_func_call_labels(TreeNode *n, Error *err, TreeNode *last,
                          int p, map<string, Class *> &classes);
void set_func_call(TreeNode *n, Error *err, TreeNode *last, int p,
                   map<string, Class *> &classes);
void set_exp_call(TreeNode *n, Error *err, TreeNode *last, int p,
                  map<string, Class *> &classes);
void set_call_pushs(TreeNode *n, Error *err, TreeNode *last, int p,
                    map<string, Class *> &classes);
void set_if(TreeNode *n, Error *err, TreeNode *last, int p);
void remove_cascate_stm(TreeNode *n, Error *err, TreeNode *last,
                        int p);

// transform varibles to addreses
void id_to_addr(TreeNode *n);

// calls to memmory sets and jumps
// TreeNode * transform_call_in_vars (TreeNode * n, Error * err,
// TreeNode * laststaS);

void set_types(TreeNode *n, Error *err,
               map<string, Class *> &classes);

void new_to_malloc(TreeNode *n, map<string, Class *> &classes);
void idf_to_bpref(TreeNode *n);
void idc_to_bpref(TreeNode *n);
void this_to_bpref(TreeNode *n);
void mark_free_std(TreeNode *n);

void while_to_jumps(TreeNode *n);
void if_to_jumps(TreeNode *n);

int temp_count = 0;

int main(int argc, char const *argv[]) {
    if (argc < 2) {
        printf("Enter: mycc <grammer_file>\n");
        return EXIT_SUCCESS;
    }

    // argmunts
    map<string, bool> arg;
    for (int i = 2; i < argc; ++i) {
        string s = argv[i];
        arg[s] = 1;
    }

    char const *lexfilename = argv[1];
    ifstream t(lexfilename);
    stringstream buffer;
    buffer << t.rdbuf();

    Lex lex;
    string code = buffer.str();
    Error *error_log = new Error(&lex, &code);

    bool error = lex.parse(code);
    string whiteStr = "WHITE";
    lex.removeToken(whiteStr);

    if (error) {
        error_log->add(lex.erroposition, lex.error);
        error_log->printall();
        return EXIT_FAILURE;
    }

    if (arg["-t"]) {
        for (unsigned int i = 0; i < lex.size(); ++i) {
            cout << i << ") ";
            cout << lex.name(lex[i]) << "\t'";
            cout << lex.text(i) << "'"
                 << "\n";
        }
        return 0;
    }

    Parser parser;
    TreeNode *result = parser.parse(lex.tokens(), lex.namesList());

    if (result == NULL) {
        error_log->add(parser.erroposition, parser.error);
        error_log->printall();
        return EXIT_FAILURE;
    }

    insert_content(result, &lex);

    if (arg["-s"]) {
        result->print();
        return 0;
    }

    // remove trash
    result->remove_token(P::LBBB);
    result->remove_token(P::RBBB);
    result->remove_token(P::LBB);
    result->remove_token(P::RBB);
    result->remove_token(P::LB);
    result->remove_token(P::RB);
    result->remove_token(P::END);
    result->remove_token(P::CLASS);
    result->remove_token(P::IG);
    result->remove_token(P::COMA);
    result->remove_token(P::PUBLIC);
    result->remove_token(P::RETURN);
    result->remove_token(P::IF);
    result->remove_token(P::WHILE);
    result->remove_token(P::ELSE);
    result->remove_token(P::DOT);

    replace_type(result, P::Expa, P::Exp);
    replace_type(result, P::Expb, P::Exp);
    replace_type(result, P::Expl, P::Exp);
    replace_type(result, P::OPA, P::Op);
    replace_type(result, P::OPB, P::Op);
    replace_type(result, P::OPC, P::Op);
    replace_type(result, P::Cond, P::StatementS);

    simplify(result);
    make_list(result, P::Formallist);
    make_list(result, P::ClassdeclS);
    make_list(result, P::StatementS);
    make_list(result, P::MethoddeclS);
    make_list(result, P::VardeclS);
    make_list(result, P::Calllist);

    if (arg["-b"]) {
        result->print();
        return 0;
    }

    map<string, Class *> classes = build_classes(result, error_log);
    result->remove_token(P::Delete);

    while_to_jumps(result);
    if_to_jumps(result);
    replace_type(result, P::Ifw, P::If);

    Enviroment *env = new Enviroment();
    creates_enviroments(result, env, classes);

    result->remove_token(P::Formallist);
    result->remove_token(P::VardeclS);
    result->remove_token(P::VOID);

    build_env_vars(result, error_log, classes);
    result->remove_token(P::Vardecl);
    remove_call_ids(result);
    result->remove_token(P::Delete);
    set_types(result, error_log, classes);

    set_func_call_labels(result, error_log, NULL, 0, classes);
    set_func_call(result, error_log, NULL, 0, classes);
    result->remove_token(P::Delete);

    if (error_log->size() != 0) {
        error_log->printall();
        return EXIT_FAILURE;
    }

    if (arg["-k"]) {
        result->print();
        return 0;
    }

    result->remove_token(P::Type);
    new_to_malloc(result, classes);
    idf_to_bpref(result);
    idc_to_bpref(result);
    this_to_bpref(result);
    mark_free_std(result);
    result->remove_token(P::Delete);

    if (error_log->size() != 0) {
        error_log->printall();
        return EXIT_FAILURE;
    }

    if (arg["-l"]) {
        result->print();
        return 0;
    }

    set_exp_call(result, error_log, NULL, 0, classes);
    remove_expressions(result);
    result->remove_token(P::Delete);

    set_if(result, error_log, NULL, 0);
    remove_cascate_stm(result, error_log, NULL, 0);
    result->remove_token(P::Delete);

    if (arg["-n"]) {
        result->print();
        return 0;
    }

    set_call_pushs(result, error_log, NULL, 0, classes);
    result->remove_token(P::Delete);

    if (error_log->size() != 0) {
        error_log->printall();
        return EXIT_FAILURE;
    }

    if (arg["-i"] && error_log->size() == 0) {
        result->print();
        return 0;
    }

    Ids ids;
    build_control_flow(result, error_log, ids, arg);

    if (error_log->size() != 0) {
        error_log->printall();
        return EXIT_FAILURE;
    }

    delete result;

    return EXIT_SUCCESS;
}

void while_to_jumps(TreeNode *n) {
    for (int i = n->size() - 1; i >= 0; i--) while_to_jumps(n->at(i));

    if (n->is(P::StatementS)) {
        for (int i = n->size() - 1; i >= 0; i--) {
            TreeNode *k = n->at(i);
            if (k->is(P::Statement) && k->get(P::While) != NULL) {
                TreeNode *stm =
                    new TreeNode(n->lextokenid, P::Statement);
                TreeNode *label =
                    new TreeNode(n->lextokenid, P::Label);
                TreeNode *label2 =
                    new TreeNode(n->lextokenid, P::Label);

                TreeNode *ifl = k->get(P::While);
                label->content = "#" + to_string(rand());
                label2->content = "#" + to_string(rand());
                n->add(stm, i);
                stm->add(label);

                TreeNode *stmS =
                    new TreeNode(n->lextokenid, P::StatementS);
                n->set(i + 1, stmS);

                stmS->add(k);
                ifl->token = P::Ifw;

                stm = new TreeNode(n->lextokenid, P::Statement);
                TreeNode *stmS2 =
                    new TreeNode(n->lextokenid, P::StatementS);
                TreeNode *jump = new TreeNode(n->lextokenid, P::Jump);
                jump->content = label->content;

                ifl->add(stmS2, 0);
                stmS2->add(stm);
                stm->add(jump);

                jump = new TreeNode(n->lextokenid, P::Jump);

                stm = new TreeNode(n->lextokenid, P::Statement);
                n->add(stm);
                stm = new TreeNode(n->lextokenid, P::Statement);

                stmS->add(stm);
                stm->add(label2);

                stm = new TreeNode(n->lextokenid, P::Statement);
                ifl->at(1)->add(stm, 0);
                stm->add(jump);
                jump->content = label2->content;
            }
        }
    }
}

void if_to_jumps(TreeNode *n) {
    for (int i = n->size() - 1; i >= 0; i--) if_to_jumps(n->at(i));

    if (n->is(P::StatementS)) {
        for (int i = n->size() - 1; i >= 0; i--) {
            TreeNode *k = n->at(i);
            if (k->is(P::Statement) && k->get(P::If) != NULL) {
                TreeNode *stm =
                    new TreeNode(n->lextokenid, P::Statement);
                TreeNode *label =
                    new TreeNode(n->lextokenid, P::Label);
                TreeNode *ifl = k->get(P::If);
                label->content = "#" + to_string(rand());
                n->add(stm, i);
                stm->add(label);

                stm = new TreeNode(n->lextokenid, P::Statement);
                TreeNode *jump = new TreeNode(n->lextokenid, P::Jump);
                jump->content = label->content;

                TreeNode *stm2 =
                    new TreeNode(n->lextokenid, P::Statement);
                TreeNode *jump2 =
                    new TreeNode(n->lextokenid, P::Jump);
                jump2->content = label->content;

                if (ifl->size() == 2) {
                    TreeNode *stmS =
                        new TreeNode(n->lextokenid, P::StatementS);
                    ifl->add(stmS, 0);
                }

                ifl->at(1)->add(stm2, 0);
                stm2->add(jump2);

                ifl->at(0)->add(stm, 0);
                stm->add(jump);
            }
        }
    }
}

void remove_call_ids(TreeNode *n) {
    for (int i = n->size() - 1; i >= 0; i--)
        remove_call_ids(n->at(i));

    if (n->is(P::Call)) {
        n->content = n->get(P::ID)->content;
        n->lextokenid = n->get(P::ID)->lextokenid;
        n->get(P::ID)->set(P::Delete);
    }
    if (n->size() == 2 && n->at(0)->is(P::ID) &&
        n->at(1)->is(P::NEW)) {
        n->at(1)->content = n->at(0)->content;
        n->at(0)->set(P::Delete);
    }
}

void set_types(TreeNode *n, Error *err,
               map<string, Class *> &classes) {
    if (n == NULL) return;

    for (int i = n->size() - 1; i >= 0; i--)
        set_types(n->at(i), err, classes);

    if (n->is(P::NUMBER))
        n->type = INT;
    else if (n->is(P::FALSE))
        n->type = BOOL;
    else if (n->is(P::TRUE))
        n->type = BOOL;
    else if (n->is(P::THIS)) {
        n->type = POINTER;
        n->clas = n->env->c;
    } else if (n->is(P::ID)) {
        // cout << n->content << "\n";
        if (n->env->f->hasarg(n->content)) {
            int p = n->env->f->vars_position[n->content];
            n->type = n->env->f->vars[p]->type;
            n->clas = n->env->f->vars[p]->c;
            n->set(P::Idf);
        } else if (n->env->hasvar(n->content)) {
            n->type = n->env->getvar(n->content)->type;
            n->clas = n->env->getvar(n->content)->c;
        } else if (n->env->c->hasvar(n->content)) {
            n->type = n->env->c->variables[n->content]->type;
            n->clas = n->env->c->variables[n->content]->c;
            n->set(P::Idc);
        } else if (classes.find(n->content) != classes.end()) {
            n->type = POINTER;
            n->clas = classes[n->content];
        } else {
            err->add(n->lextokenid, "Undeclered variable.");
            return;
        }
    } else if (n->is(P::Call)) {
        string str = n->content;
        TreeNode *exp = n->get(P::Exp);
        if (exp == NULL) {
            // search method in class
            if (n->env->c->hasmet(str)) {
                n->type = n->env->c->methods[str]->return_type;
                n->clas = n->env->c->methods[str]->return_class;
            } else {
                err->add(n->lextokenid, "Unknow method.");
                return;
            }
        } else if (exp->type != POINTER) {
            err->add(n->lextokenid - 2, "Unknow ref.");
            return;
        } else {
            Class *s = exp->clas;
            if (s->hasmet(str)) {
                n->type = exp->clas->methods[str]->return_type;
                n->clas = exp->clas->methods[str]->return_class;
            } else {
                err->add(n->lextokenid, "Unknow method in class.");
                return;
            }
        }
    } else if (n->is(P::Exp)) {
        if (n->size() == 1) {
            n->type = n->at(0)->type;
            n->clas = n->at(0)->clas;
        } else if (n->size() == 3 && n->at(1)->is(P::Op)) {
            if (n->at(0)->type != n->at(2)->type ||
                n->at(0)->clas != n->at(2)->clas) {
                err->add(n->at(1)->lextokenid,
                         "Operation with diferent types!");
                return;
            }
            if (n->at(0)->clas != NULL) {
                err->add(n->at(1)->lextokenid,
                         "Can't operate class!");
                return;
            }
            if (n->at(1)->content.compare("<") == 0 ||
                n->at(1)->content.compare("&&") == 0) {
                n->type = BOOL;
            } else {
                n->type = n->at(0)->type;
            }
        } else if (n->size() == 2 && n->at(1)->is(P::EXCL)) {
            if (n->at(0)->type != BOOL) {
                err->add(
                    n->at(1)->lextokenid,
                    "Can negate something diferrent from boolean!");
                return;
            }
            n->type = BOOL;
        } else {
            err->add(n->lextokenid, "Unknow exp type.");
            return;
        }
    } else if (n->is(P::NEW)) {
        // check if exist class with this name
        string id = n->content;
        map<string, Class *>::iterator it = classes.find(id);
        if (it == classes.end()) {
            err->add(n->at(0)->lextokenid, "Unknow token.");
            return;
        }
        n->type = POINTER;
        n->clas = classes[id];
    } else if (n->is(P::Setvalue)) {
        // check if exist class with this name
        if (n->at(0)->type != n->at(1)->type ||
            n->at(0)->clas != n->at(1)->clas)
            err->add(n->at(1)->lextokenid + 1, "Wrong assign type.");
    } else if (n->is(P::If) || n->is(P::While)) {
        // check if exist class with this name
        if (n->get(P::Exp)->type != BOOL)
            err->add(n->lextokenid, "Argument must be an boolean.");
    } else if (n->is(P::Ret)) {
        // check if exist class with this name
        if (n->size() == 1 &&
            n->get(P::Exp)->type != n->env->f->return_type) {
            err->add(n->lextokenid,
                     "Return type different from method type.");
            return;
        }
        if (n->size() == 0 && n->env->f->return_type != VOID) {
            err->add(n->lextokenid, "This is not an void function!");
            return;
        }
        if (n->size() == 1) n->env->f->has_return_declared = true;
    } else if (n->is(P::Methoddecl)) {
        // check if exist class with this name
        if (!n->env->f->has_return_declared &&
            n->env->f->return_type != VOID)
            err->add(n->lextokenid, "This function doesn't return!");
    } else {
        n->type = NONE;
    }
}

void new_to_malloc(TreeNode *n, map<string, Class *> &classes) {
    for (int i = n->size() - 1; i >= 0; i--)
        new_to_malloc(n->at(i), classes);

    if (n->is(P::NEW)) {
        n->set(P::Malloc);
        n->content = to_string(classes[n->content]->size);
    }
}

void idf_to_bpref(TreeNode *n) {
    for (int i = n->size() - 1; i >= 0; i--) idf_to_bpref(n->at(i));

    if (n->is(P::Idf)) {
        TreeNode *nnum = new TreeNode(n->lextokenid, P::NUMBER);
        TreeNode *exp = new TreeNode(n->lextokenid, P::Exp);
        TreeNode *exp1 = new TreeNode(n->lextokenid, P::Exp);
        TreeNode *exp2 = new TreeNode(n->lextokenid, P::Exp);
        TreeNode *nplus = new TreeNode(n->lextokenid, P::Op);
        TreeNode *nfp = new TreeNode(n->lextokenid, P::ID);

        n->set(P::Men);
        nplus->content = "+";
        nfp->content = "$BP";

        nnum->type = POINTER;
        nfp->type = POINTER;
        exp->type = POINTER;
        exp1->type = POINTER;
        exp2->type = POINTER;

        // compute position
        // 8 for old epb, 8 for class pointer 8 for value
        int pos = 24;
        int p = n->env->f->vars_position[n->content];
        for (int i = n->env->f->vars.size() - 1; i > p; --i) {
            pos += n->env->f->vars[i]->size;
        }
        nnum->content = to_string(pos);

        n->add(exp);

        exp->add(exp1);
        exp1->add(nnum);
        exp->add(nplus);
        exp->add(exp2);
        exp2->add(nfp);
    }
}

void idc_to_bpref(TreeNode *n) {
    for (int i = n->size() - 1; i >= 0; i--) idc_to_bpref(n->at(i));

    if (n->is(P::Idc)) {
        TreeNode *nnum = new TreeNode(n->lextokenid, P::NUMBER);
        TreeNode *exp = new TreeNode(n->lextokenid, P::Exp);
        TreeNode *exp1 = new TreeNode(n->lextokenid, P::Exp);
        TreeNode *exp2 = new TreeNode(n->lextokenid, P::Exp);
        TreeNode *nplus = new TreeNode(n->lextokenid, P::Op);
        TreeNode *thi = new TreeNode(n->lextokenid, P::THIS);

        nnum->type = POINTER;
        thi->type = POINTER;
        exp->type = POINTER;
        exp1->type = POINTER;
        exp2->type = POINTER;

        n->set(P::Men);
        nplus->content = "+";
        // conpute position
        nnum->content =
            to_string(n->env->c->variables_pos[n->content]);

        n->add(exp);

        exp->add(exp1);
        exp1->add(nnum);
        exp->add(nplus);
        exp->add(exp2);
        exp2->add(thi);
    }
}

void this_to_bpref(TreeNode *n) {
    for (int i = n->size() - 1; i >= 0; i--) this_to_bpref(n->at(i));

    if (n->is(P::THIS)) {
        TreeNode *nnum = new TreeNode(n->lextokenid, P::NUMBER);
        TreeNode *exp = new TreeNode(n->lextokenid, P::Exp);
        TreeNode *exp1 = new TreeNode(n->lextokenid, P::Exp);
        TreeNode *exp2 = new TreeNode(n->lextokenid, P::Exp);
        TreeNode *nplus = new TreeNode(n->lextokenid, P::Op);
        TreeNode *thi = new TreeNode(n->lextokenid, P::ID);
        thi->content = "$BP";

        nnum->type = POINTER;
        thi->type = POINTER;
        exp1->type = POINTER;
        exp2->type = POINTER;

        n->set(P::Men);
        nplus->content = "+";
        // conpute position
        nnum->content = to_string(8 + 8);

        n->add(exp);

        exp->add(exp1);
        exp1->add(nnum);
        exp->add(nplus);
        exp->add(exp2);
        exp2->add(thi);
    }
}

void mark_free_std(TreeNode *n) {
    for (int i = n->size() - 1; i >= 0; i--) mark_free_std(n->at(i));

    if (n->is(P::Statement) && n->size() == 0) n->set(P::Delete);
}

// Somente coisas desse tipo
// -methoddeclS
// | methoddecl methoddeclS
// | methoddecl
// |
void make_list(TreeNode *n, P::Token s) {
    for (int i = n->size() - 1; i >= 0; i--) make_list(n->at(i), s);

    if (n->is(s) && n->size() == 2) {
        TreeNode *next = n->at(0);
        TreeNode *add = n->at(1);
        n->clear();

        for (int i = 0; i < next->size(); i++) n->add(next->at(i));
        n->add(add);
    }
}

void creates_enviroments(TreeNode *n, Enviroment *env,
                         map<string, Class *> &classes) {
    if (n == NULL) return;
    n->env = env;

    for (int i = n->size() - 1; i >= 0; i--) {
        TreeNode *k = n->at(i);
        if (k->is(P::StatementS))
            creates_enviroments(k, env->clone(), classes);
        // else if (k->is(P::Mainclass))
        //     creates_enviroments(k, env->clone(), classes);
        else if (k->is(P::Methoddecl)) {
            env = env->clone();
            env->f = env->c->methods[k->content];
            creates_enviroments(k, env->clone(), classes);
        } else if (k->is(P::Classdecl)) {
            env = env->clone();
            env->c = classes[k->content];
            creates_enviroments(k, env->clone(), classes);
        } else
            creates_enviroments(k, env, classes);
    }
}

Variable *get_from_vardecl(TreeNode *n, map<string, Class *> &list) {
    Variable *v = NULL;
    if (n->get(P::Type)->at(0)->is(P::INT)) {
        v = new Variable(INT, NULL);
    } else if (n->get(P::Type)->at(0)->is(P::BOOLEAN)) {
        v = new Variable(BOOL, NULL);
    } else if (n->get(P::Type)->at(0)->is(P::ID)) {
        Class *c = list[n->get(P::Type)->at(0)->content];
        v = new Variable(POINTER, c);
    }
    return v;
}

Function *get_from_methoddecl(TreeNode *n,
                              map<string, Class *> &list) {
    Function *f = new Function();
    TreeNode *varlist = n->get(P::Formallist);
    for (int i = 0; i < varlist->size(); i++) {
        TreeNode *var = varlist->at(i);
        f->addarg(var->get(P::ID)->content,
                  get_from_vardecl(var, list));
    }
    if (n->get(P::VOID) != NULL) {
        f->return_type = VOID;
    } else if (n->get(P::Type)->at(0)->is(P::INT)) {
        f->return_type = INT;
    } else if (n->get(P::Type)->at(0)->is(P::BOOLEAN)) {
        f->return_type = BOOL;
    } else if (n->get(P::Type)->at(0)->is(P::ID)) {
        f->return_type = POINTER;
        f->return_class = list[n->get(P::Type)->get(P::ID)->content];
    }
    f->statment = n;
    return f;
}

map<string, Class *> build_classes(TreeNode *n, Error *err) {
    map<string, Class *> list;

    TreeNode *classes = n->get(P::ClassdeclS);

    for (int i = 0; i < classes->size(); i++) {
        TreeNode *k = classes->at(i);
        TreeNode *id = k->get(P::ID);

        Class *clas = new Class();
        list[id->content] = clas;
    }

    for (int i = 0; i < classes->size(); i++) {
        TreeNode *k = classes->at(i);
        TreeNode *id = k->get(P::ID);
        TreeNode *variables = k->get(P::VardeclS);
        TreeNode *methods = k->get(P::MethoddeclS);
        Class *clas = list[id->content];

        for (int j = 0; j < variables->size(); j++) {
            TreeNode *var = variables->at(j);
            clas->addvar(var->get(P::ID)->content,
                         get_from_vardecl(var, list));
        }

        for (int j = 0; j < methods->size(); j++) {
            TreeNode *fun = methods->at(j);
            Function *f = get_from_methoddecl(fun, list);
            clas->addmethod(fun->get(P::ID)->content, f);

            fun->content = fun->get(P::ID)->content;
            fun->lextokenid = fun->get(P::ID)->lextokenid;
            fun->get(P::ID)->set(P::Delete);
            fun->clas = clas;
        }
        k->content = id->content;
        id->set(P::Delete);
    }
    return list;
}

void insert_content(TreeNode *n, Lex *lex) {
    if (n->lextokenid != -1)
        n->content = lex->text(n->lextokenid);
    else
        n->content = "";

    for (int i = 0; i < n->size(); i++) {
        insert_content(n->at(i), lex);
    }
}

void insert_var(TreeNode *k, Enviroment *env, Error *err,
                map<string, Class *> &classes) {
    Variable *v = get_from_vardecl(k, classes);
    string name = k->get(P::ID)->content;  // ID

    if (env->f->hasarg(name)) {
        // check if variable exist in function
        err->add(k->at(0)->lextokenid,
                 "Variable declared in function arguments.");
    } else if (!env->addvar(name, v)) {
        // check if variable exist in enviromente
        err->add(k->at(0)->lextokenid,
                 "Variable declared multiple times.");
    } else if (env->c->hasvar(name)) {
        err->add(k->at(0)->lextokenid, "Variable declared in class.");
    }
}

void build_env_vars(TreeNode *n, Error *err,
                    map<string, Class *> &classes) {
    if (n == NULL) return;

    if (n->is(P::Vardecl)) {
        insert_var(n, n->env, err, classes);
    }

    for (int i = n->size() - 1; i >= 0; i--) {
        TreeNode *k = n->at(i);
        build_env_vars(k, err, classes);
    }
}

void replace_type(TreeNode *n, P::Token from, P::Token to) {
    if (n->is(from)) n->set(to);

    for (int i = n->size() - 1; i >= 0; i--)
        replace_type(n->at(i), from, to);
}

TreeNode *simplify(TreeNode *node) {
    if (node == NULL) return NULL;
    for (int i = 0; i < node->size(); i++) {
        TreeNode *t = simplify(node->at(i));
        node->set(i, t);
    }

    if (node->size() == 1 && node->same_content(node->at(0)) &&
        node->same_type(node->at(0)) && !node->is(P::Type)) {
        TreeNode *t = node->at(0);
        return t;
    }
    return node;
}

void set_if(TreeNode *n, Error *err, TreeNode *last, int p) {
    if (n == NULL) return;

    if (n->is(P::StatementS)) {
        last = n;

        for (int i = 0; i < n->size(); i++) {
            TreeNode *k = n->at(i);
            set_if(k, err, last, i);
        }
        return;
    } else if (n->is(P::If) && n->get(P::Delete) == NULL) {
        TreeNode *false_stm = n->at(0);
        TreeNode *true_stm = n->at(1);
        string label_true =
            true_stm->at(true_stm->size() - 1)->at(0)->content;

        for (int i = false_stm->size() - 1; i >= 0; i--) {
            TreeNode *k = false_stm->at(i);
            last->add(k, p);
            // p++;
        }

        // cout << n->tokenstring << last << "\n";
        for (int i = true_stm->size() - 1; i >= 0; i--) {
            TreeNode *k = true_stm->at(i);
            last->add(k, p);
            // p++;
        }

        TreeNode *d1 = new TreeNode(0, P::Delete);
        TreeNode *d2 = new TreeNode(0, P::Delete);

        n->set(0, d1);
        n->set(1, d2);

        TreeNode *str = new TreeNode(0, P::To);
        str->content = label_true;
        n->add(str, 0);
    }

    for (int i = 0; i < n->size(); i++) {
        TreeNode *k = n->at(i);
        set_if(k, err, last, p);
    }
}

void remove_cascate_stm(TreeNode *n, Error *err, TreeNode *last,
                        int p) {
    if (n == NULL) return;

    if (n->is(P::StatementS)) {
        for (int i = 0; i < n->size(); i++) {
            TreeNode *k = n->at(i);
            remove_cascate_stm(k, err, n, i);
        }
    } else {
        for (int i = 0; i < n->size(); i++) {
            TreeNode *k = n->at(i);
            remove_cascate_stm(k, err, last, p);
        }
    }

    if (n->is(P::StatementS) && last != NULL) {
        for (int i = n->size() - 1; i >= 0; i--) {
            TreeNode *k = n->at(i);
            last->add(k, p);
            // p++;
        }
        n->set(P::Delete);
    }
}

void set_func_call_labels(TreeNode *n, Error *err, TreeNode *last,
                          int p, map<string, Class *> &classes) {
    if (n == NULL) return;

    if (n->is(P::StatementS)) {
        last = n;

        TreeNode *stm = new TreeNode(n->lextokenid, P::Statement);
        TreeNode *label = new TreeNode(n->lextokenid, P::Label);
        label->content = "#" + to_string(rand());
        n->add(stm);
        stm->add(label);

        for (int i = 0; i < n->size(); i++) {
            TreeNode *k = n->at(i);
            set_func_call_labels(k, err, last, i, classes);
        }
        return;
    }

    for (int i = 0; i < n->size(); i++) {
        TreeNode *k = n->at(i);
        set_func_call_labels(k, err, last, p, classes);
    }
}

void set_func_call(TreeNode *n, Error *err, TreeNode *last, int p,
                   map<string, Class *> &classes) {
    if (n == NULL) return;

    if (n->is(P::StatementS)) {
        last = n;
        for (int i = 0; i < n->size(); i++) {
            TreeNode *k = n->at(i);
            set_func_call(k, err, last, i, classes);
        }
        return;
    } else if (n->is(P::Calllist)) {
        for (int i = n->size() - 1; i >= 0; i--) {
            TreeNode *statement2 =
                new TreeNode(n->lextokenid, P::Statement);
            TreeNode *varset =
                new TreeNode(n->lextokenid, P::Setvalue);

            TreeNode *id = new TreeNode(n->lextokenid, P::ID);
            TreeNode *id2 = new TreeNode(n->lextokenid, P::ID);

            id->type = n->at(i)->type;
            id2->type = n->at(i)->type;

            id->content = "$" + to_string(temp_count++);
            id2->content = id->content;

            Variable *v =
                new Variable(n->at(i)->type, n->at(i)->clas);

            last->env->addvar(id->content, v);

            last->add(statement2, p + 1);

            statement2->add(varset);
            varset->add(n->at(i));
            varset->add(id2);
            n->set(i, id);
        }
    } else if (n->is(P::Call)) {
        Class *c = NULL;
        if (n->size() == 2)
            c = n->at(1)->clas;
        else if (n->size() == 1)
            c = n->env->c;

        if (c != NULL && c->hasmet(n->content)) {
            Function *f = c->methods[n->content];
            if (f->vars.size() !=
                (unsigned int)n->get(P::Calllist)->size()) {
                err->add(n->lextokenid,
                         "Wrong number of args, expecting " +
                             to_string(f->vars.size()) + ".");
                return;
            }
            for (unsigned int i = 0; i < f->vars.size(); ++i) {
                if (f->vars[i]->type !=
                        n->get(P::Calllist)->at(i)->type ||
                    f->vars[i]->c !=
                        n->get(P::Calllist)->at(i)->clas) {
                    err->add(n->lextokenid + 2 * i,
                             "Wrong argument type.");
                    return;
                }
            }
            // put addr as first function arg
            if (n->size() == 2) {
                TreeNode *node = new TreeNode(0, P::Delete);
                n->get(P::Calllist)->add(n->at(1));
                n->set(1, node);
            } else if (n->size() == 1) {
                TreeNode *node = new TreeNode(
                    n->get(P::Calllist)->lextokenid, P::THIS, n);
                node->content = "this";
                node->clas = c;
                node->type = POINTER;
                n->get(P::Calllist)->add(node);
            }
            TreeNode *tmp = f->statment->get(P::StatementS);
            n->content = tmp->at(tmp->size() - 1)->at(0)->content;
        } else {
            err->add(n->lextokenid, "Problem....");
            return;
        }
    }

    for (int i = 0; i < n->size(); i++) {
        TreeNode *k = n->at(i);
        set_func_call(k, err, last, p, classes);
    }
}

void set_exp_call(TreeNode *n, Error *err, TreeNode *last, int p,
                  map<string, Class *> &classes) {
    if (n == NULL) return;

    if (n->is(P::StatementS)) {
        last = n;
        for (int i = 0; i < n->size(); i++) {
            TreeNode *k = n->at(i);
            set_exp_call(k, err, last, i, classes);
        }
        return;
    } else {
        for (int i = n->size() - 1; i >= 0; i--) {
            if (!n->at(i)->is(P::Exp)) continue;
            TreeNode *exp = n->at(i);
            TreeNode *statement2 =
                new TreeNode(n->lextokenid, P::Statement);
            TreeNode *varset =
                new TreeNode(n->lextokenid, P::Setvalue);

            TreeNode *id = new TreeNode(n->lextokenid, P::ID);
            TreeNode *id2 = new TreeNode(n->lextokenid, P::ID);

            id->type = n->at(i)->type;
            id2->type = n->at(i)->type;

            id->content = "$" + to_string(temp_count++);
            id2->content = id->content;

            Variable *v = new Variable(exp->type, exp->clas);

            last->env->addvar(id->content, v);

            last->add(statement2, p + 1);

            statement2->add(varset);
            varset->add(exp);
            varset->add(id2);
            n->set(i, id);
            exp->set(P::Exp2);
        }
    }

    for (int i = 0; i < n->size(); i++) {
        TreeNode *k = n->at(i);
        set_exp_call(k, err, last, p, classes);
    }
}

void remove_expressions(TreeNode *n) {
    if (n->is(P::Setvalue)) n->set(P::Move);
    for (int i = n->size() - 1; i >= 0; i--) {
        TreeNode *k = n->nodes[i];
        if (k->nodes.size() == 1 && k->is(P::Exp2)) {
            n->nodes[i] = k->nodes[0];
        }
        if (k->nodes.size() == 3 && k->is(P::Exp2)) {
            TreeNode *op = k->nodes[1];
            op->add(k->nodes[0]);
            op->add(k->nodes[2]);
            n->nodes[i] = op;
        }
        if (k->nodes.size() == 2 && k->is(P::Exp2) &&
            k->get(P::EXCL) != NULL) {
            k->get(P::EXCL)->set(P::Delete);
            k->set(P::Neg);
        }
        remove_expressions(n->nodes[i]);
    }
}

void set_call_pushs(TreeNode *n, Error *err, TreeNode *last, int p,
                    map<string, Class *> &classes) {
    if (n == NULL) return;

    if (n->is(P::StatementS)) {
        last = n;
        for (int i = 0; i < n->size(); i++) {
            TreeNode *k = n->at(i);
            set_call_pushs(k, err, last, i, classes);
        }
        return;
    } else if (n->is(P::Calllist)) {
        TreeNode *statement =
            new TreeNode(n->lextokenid, P::Statement);
        TreeNode *push = new TreeNode(n->lextokenid, P::Pushall);

        TreeNode *statement2 =
            new TreeNode(n->lextokenid, P::Statement);
        TreeNode *pop = new TreeNode(n->lextokenid, P::Popall);

        last->add(statement2, p);
        p++;
        last->add(statement, p + 1);
        statement->add(push);
        statement2->add(pop);

        for (int i = 0; i < n->size(); i++) {
            statement = new TreeNode(n->lextokenid, P::Statement);
            push = new TreeNode(n->lextokenid, P::Push);

            statement2 = new TreeNode(n->lextokenid, P::Statement);
            pop = new TreeNode(n->lextokenid, P::Popd);

            last->add(statement2, p);
            p++;
            last->add(statement, p + 1);
            statement->add(push);
            push->add(n->at(i));
            statement2->add(pop);

            n->set(P::Delete);
        }
    }

    for (int i = 0; i < n->size(); i++) {
        TreeNode *k = n->at(i);
        set_call_pushs(k, err, last, p, classes);
    }

    if (n->is(P::Statement) && n->get(P::Call) != NULL) {
        TreeNode *call = n->get(P::Call);
        // add a pop to register if is not a void function
        if (call->type != VOID) {
            TreeNode *statement =
                new TreeNode(n->lextokenid, P::Statement);
            TreeNode *pop = new TreeNode(n->lextokenid, P::Getret);
            TreeNode *id = new TreeNode(n->lextokenid, P::ID);
            id->type = call->type;

            last->add(statement, p);
            statement->add(pop);
            id->content = "$" + to_string(temp_count++);
            pop->add(id);

            call->type = VOID;
        }
    }
    if (n->is(P::Statement) && n->get(P::Move) &&
        n->get(P::Move)->get(P::Call)) {
        TreeNode *call = n->get(P::Move)->get(P::Call);
        // add a pop to register if is not a void function
        if (call->type != VOID) {
            TreeNode *statement =
                new TreeNode(n->lextokenid, P::Statement);
            TreeNode *pop = new TreeNode(n->lextokenid, P::Getret);

            last->add(statement, p);
            statement->add(pop);
            pop->add(n->get(P::Move)->at(1));

            n->nodes.clear();
            n->add(call);
            call->type = VOID;
        }
    }
}
