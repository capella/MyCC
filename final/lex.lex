
CLASS: class
VOID: void
MAIN: public \WHITE static \WHITE \VOID \WHITE  main
LB: {
RB: }
LBB: \(
RBB: \)
LBBB: [
RBBB: ]
END: ;
IG: =
RETURN: return
COMA: ,
PUBLIC: public
//STRING: String
EXTENDS: extends
BOOLEAN: boolean
DOT: .
LENGTH: length
INT: int
WHILE: while
IF: if
ELSE: else
THIS: this
FALSE: false
TRUE: true
PRINT: print
NEW: new
EXCL: !
ID: \LETTER(\LETTER|\DIGITS|_)*
NUMBER: \DIGITS\DIGITS*
DOUBLE: \NUMBER.\NUMBER
OPA: && | <
OPB: + | - 
OPC: \* | / | %

// Constants
LETTERU: a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|x|y|z|w
LETTERD: A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|X|Y|Z|W
LETTER: \LETTERU|\LETTERD
DIGITS: 1|2|3|4|5|6|7|8|9|0
WHITE: (\s|\n|\t)*|//(\LETTER|\DIGITS|\LB|\RB|\LBB|\RBB|\LBBB|\RBBB|\s|\t|\END|\OPA|\OPC|\OPB|\DOT|\COMA)*\n
