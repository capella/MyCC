////////////////////////////////////////////////////////////// */
//
// Author: Gabriel Capella
// Date: 2015-10-13
//
////////////////////////////////////////////////////////////// */

#include "toinstructions.h"

#define VARSET map<Instruction *, set<int> >

static int max_delta = 0;

static void LivenessAnalysis(InstructionGraph &all, VARSET &in,
                             VARSET &out);
static map<int, int> Main(InstructionGraph &all, int delta, Ids &ids);
static void spill(InstructionGraph &all, int var, int delta,
                  int tmp_var);

static TreeNode *get_main(TreeNode *n, Error *err) {
    TreeNode *main_stm = NULL;

    stack<TreeNode *> stack;
    stack.push(n);

    // get main
    while (!stack.empty()) {
        TreeNode *node = stack.top();
        stack.pop();

        if (node->is(P::Methoddecl) &&
            !node->content.compare("main")) {
            if (main_stm != NULL) {
                err->add(node->lextokenid,
                         "Multiple main function found!");
                err->add(main_stm->lextokenid,
                         "Multiple main function found!");
                return NULL;
            }
            main_stm = node;
        }

        for (int i = node->size() - 1; i >= 0; i--)
            stack.push(node->at(i));
    }

    if (main_stm == NULL) {
        err->add(-1, "No main function found!");
        return NULL;
    }

    return main_stm;
}

// receive one stm node and return an Instruction
static Instruction *stm_to_instruction(TreeNode *n, Ids &ids,
                                       Ids &labels) {
    TreeNode *first = n->at(0);

    if (first->is(P::Call)) {
        // CallVoid
        Call *call = new Call();
        call->to_name = first->content;
        return call;
    } else if (first->is(P::Pushall)) {
        return new PushAll();
    } else if (first->is(P::Popall)) {
        return new PopAll();
    } else if (first->is(P::Push)) {
        // MoveCall
        Push *mm = new Push();
        mm->from = ids.get(first->get(P::ID)->content);
        ids.set_size(mm->from, getsize(first->get(P::ID)->type));
        return mm;
    } else if (first->is(P::Pop)) {
        // MoveCall
        Pop *mm = new Pop();
        mm->to = ids.get(first->get(P::ID)->content);
        ids.set_size(mm->to, getsize(first->get(P::ID)->type));
        return mm;
    } else if (first->is(P::Getret)) {
        // MoveCall
        GetRet *mm = new GetRet();
        mm->to = ids.get(first->get(P::ID)->content);
        ids.set_size(mm->to, getsize(first->get(P::ID)->type));
        return mm;
    } else if (first->is(P::Move) && first->get(P::Malloc) != NULL) {
        // MovMalloc
        MovMalloc *malloc = new MovMalloc();
        malloc->amount = stoi(first->get(P::Malloc)->content);
        malloc->to = ids.get(first->get(P::ID)->content);
        ids.set_size(malloc->to, getsize(first->get(P::ID)->type));
        return malloc;
    } else if (first->is(P::Move) && first->size() == 2 &&
               first->at(0)->is(P::ID) && first->at(1)->is(P::ID)) {
        // MoveMove
        MoveMove *mm = new MoveMove();
        mm->to = ids.get(first->at(1)->content);
        mm->from = ids.get(first->at(0)->content);
        ids.set_size(mm->from, getsize(first->at(0)->type));
        ids.set_size(mm->to, getsize(first->at(1)->type));
        return mm;
    } else if (first->is(P::Move) && first->size() == 2 &&
               first->at(0)->is(P::NUMBER) &&
               first->at(1)->is(P::ID)) {
        // MoveMove
        MoveLiteral *mm = new MoveLiteral();
        mm->to = ids.get(first->at(1)->content);
        mm->value = stoi(first->at(0)->content);
        ids.set_size(mm->to, getsize(first->at(1)->type));
        return mm;
    } else if (first->is(P::Move) && first->size() == 2 &&
               first->at(0)->is(P::TRUE) && first->at(1)->is(P::ID)) {
        // MoveMove
        MoveLiteral *mm = new MoveLiteral();
        mm->to = ids.get(first->at(1)->content);
        mm->value = 1;
        ids.set_size(mm->to, getsize(first->at(1)->type));
        return mm;
    } else if (first->is(P::Move) && first->size() == 2 &&
               first->at(0)->is(P::FALSE) &&
               first->at(1)->is(P::ID)) {
        // MoveMove
        MoveLiteral *mm = new MoveLiteral();
        mm->to = ids.get(first->at(1)->content);
        mm->value = 0;
        ids.set_size(mm->to, getsize(first->at(1)->type));
        return mm;
    } else if (first->is(P::Move) && first->size() == 2 &&
               first->at(0)->is(P::Op) && first->at(1)->is(P::ID)) {
        // MoveMove
        MoveOp *mm = new MoveOp();
        mm->to = ids.get(first->at(1)->content);
        ids.set_size(mm->to, getsize(first->at(1)->type));

        if (first->at(0)->content.compare("*") == 0)
            mm->operation = MULT;
        if (first->at(0)->content.compare("+") == 0)
            mm->operation = PLUS;
        if (first->at(0)->content.compare("-") == 0)
            mm->operation = MINUS;
        if (first->at(0)->content.compare("/") == 0)
            mm->operation = DIV;
        if (first->at(0)->content.compare("<") == 0)
            mm->operation = LESS;
        if (first->at(0)->content.compare("&&") == 0)
            mm->operation = AND;
        if (first->at(0)->content.compare("%") == 0)
            mm->operation = MOD;

        mm->B = ids.get(first->at(0)->at(0)->content);
        mm->A = ids.get(first->at(0)->at(1)->content);
        ids.set_size(mm->B, getsize(first->at(0)->at(0)->type));
        ids.set_size(mm->A, getsize(first->at(0)->at(1)->type));

        return mm;
    } else if (first->is(P::Move) && first->size() == 2 &&
               first->at(0)->is(P::Men) && first->at(1)->is(P::ID)) {
        // MoveMove
        MenToReg *mm = new MenToReg();
        mm->to = ids.get(first->at(1)->content);
        mm->from = ids.get(first->at(0)->at(0)->content);

        ids.set_size(mm->to, getsize(first->at(1)->type));
        ids.set_size(mm->from, getsize(first->at(0)->at(0)->type));

        return mm;
    } else if (first->is(P::Move) && first->size() == 2 &&
               first->at(0)->is(P::ID) && first->at(1)->is(P::Men)) {
        // AQUI
        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        RegToMen *mm = new RegToMen();
        mm->to = ids.get(first->at(1)->at(0)->content);
        mm->from = ids.get(first->at(0)->content);

        ids.set_size(mm->to, getsize(first->at(1)->at(0)->type));
        ids.set_size(mm->from, getsize(first->at(1)->type));

        return mm;
    } else if (first->is(P::Move) && first->size() == 2 &&
               first->at(0)->is(P::Neg) && first->at(1)->is(P::ID)) {
        // MoveMove
        MoveNeg *mm = new MoveNeg();
        mm->to = ids.get(first->at(1)->content);
        mm->from = ids.get(first->at(0)->at(0)->content);

        ids.set_size(mm->from, getsize(first->at(0)->at(0)->type));
        ids.set_size(mm->to, getsize(first->at(1)->type));

        return mm;
    } else if (first->is(P::Ret)) {
        // MoveMove
        SetRet *mm = new SetRet();
        if (first->size() == 1)
            mm->from = ids.get(first->at(0)->content);
        else
            mm->from = -1;

        ids.set_size(mm->from, 8);

        return mm;
    } else if (first->is(P::If)) {
        // JumpCond
        JumpCond *mm = new JumpCond();
        mm->value = ids.get(first->at(1)->content);
        mm->to = labels.get(first->at(0)->content);

        return mm;
    } else if (first->is(P::Jump)) {
        // JumpCond
        Jump *mm = new Jump();
        mm->to = labels.get(first->content);

        return mm;
    } else if (n->get(P::PRINT) != NULL) {
        Print *print = new Print();
        print->from = ids.get(n->at(0)->content);

        ids.set_size(print->from, getsize(n->at(0)->type));
        return print;
    } else if (n->get(P::Label) != NULL) {
        Label *label = new Label();
        label->url = labels.get(n->at(0)->content);
        return label;
    } else if (n->get(P::Popd) != NULL) {
        PopDiscart *popd = new PopDiscart();
        return popd;
    }
    return NULL;
}

void build_control_flow(TreeNode *n, Error *err, Ids &ids,
                        map<string, bool> &arg) {
    map<int, Label *>
        maplabels;  // givem one label id, get it instruction
    Ids labels;
    set<int> defines;

    TreeNode *main_stmS = get_main(n, err);
    InstructionGraph graph;

    if (!main_stmS) return;

    // __LABEL0: will be main
    TreeNode *aux = main_stmS->at(0);
    aux = aux->at(aux->size() - 1);
    labels.get(aux->at(0)->content);

    stack<TreeNode *> stackt;
    stackt.push(n);

    Instruction *last = NULL;

    // Create sequency flow
    while (!stackt.empty()) {
        TreeNode *node = stackt.top();
        stackt.pop();

        if (node->is(P::Statement) && node->size() > 0) {
            Instruction *inst = stm_to_instruction(node, ids, labels);

            if (inst == NULL) {
                if (node->at(0)->is(P::Move) &&
                    node->at(0)->size() == 2 &&
                    node->at(0)->at(0)->is(P::Call) &&
                    node->at(0)->at(1)->is(P::ID)) {
                    err->add(
                        node->at(0)->lextokenid,
                        "Cannot get value from a void function\n");
                    return;
                }
                node->print();
                err->add(node->lextokenid,
                         "Unable to decode instruction - this is an "
                         "compiler error!\n");
                return;
            } else if (Label *l = dynamic_cast<Label *>(inst)) {
                maplabels[l->url] = l;
            }

            graph.add(inst);

            if (last != NULL) graph.add_edge(last, inst);

            if (dynamic_cast<Jump *>(inst))
                last = NULL;
            else
                last = inst;

            vector<int> def = inst->def();
            for (int a : def) defines.insert(a);

        } else {
            if (!node->is(P::Statement)) {
                last = NULL;
                defines.clear();
            }

            for (int i = 0; i < node->size(); i++)
                stackt.push(node->at(i));
        }
    }

    if (err->size() != 0) return;

    // add junps link
    vector<Instruction *> nodes = graph.nodes();
    for (Instruction *inst : nodes) {
        if (Jump *j = dynamic_cast<Jump *>(inst)) {
            graph.add_edge(inst, maplabels[j->to]);
        } else if (JumpCond *j = dynamic_cast<JumpCond *>(inst)) {
            graph.add_edge(inst, maplabels[j->to]);
        } else if (Call *j = dynamic_cast<Call *>(inst)) {
            j->to_id = labels.get(j->to_name);
        }
    }

    if (arg["-y"]) {
        graph.print();
        return;
    }

    map<int, int> colors = Main(graph, MEM_POS_SIZE, ids);

    if (arg["-x"]) {
        graph.print();
        return;
    }
    build(colors, graph, main_stmS->clas->size, max_delta);
}

static void LivenessAnalysis(InstructionGraph &graph, VARSET &in,
                             VARSET &out) {
    // Liveness calculation
    VARSET inl, outl;
    unsigned int count = 0;
    vector<Instruction *> nodes = graph.nodes();
    while (count != nodes.size()) {
        for (unsigned int i = 0;
             i < nodes.size() && count != nodes.size(); i++) {
            Instruction *n = nodes[i];

            vector<int> def = n->def();
            vector<int> use = n->use();

            inl[n] = in[n];
            outl[n] = out[n];

            in[n].clear();
            // isso doi na complexidade
            set_difference(out[n].begin(), out[n].end(), def.begin(),
                           def.end(), inserter(in[n], in[n].begin()));
            in[n].insert(use.begin(), use.end());

            // ui
            out[n].clear();
            for (auto s : graph.eddge_to(n))
                out[n].insert(in[s].begin(), in[s].end());

            // isso doi na complexidade
            if (inl[n] == in[n] && outl[n] == out[n]) {
                count++;
            } else {
                count = 0;
            }
        }
    }
    // for (auto n: nodes) {
    //     cout << n->name() << "\n\tIN\t";
    //     for (auto k: in[n]) cout << k << " ";
    //     cout << "\n\tOUT\t";
    //     for (auto k: out[n]) cout << k << " ";
    //     // cout << "\n\tDEF\t";
    //     // for (auto k: n->def()) cout << k << " ";
    //     // cout << "\n\tUSE\t";
    //     // for (auto k: n->use()) cout << k << " ";
    //     cout << "\n";
    // }
}

Graph<int> Build(InstructionGraph &graph, VARSET &in, VARSET &out) {
    Graph<int> g;

    vector<Instruction *> nodes = graph.nodes();
    for (unsigned int i = 0; i < nodes.size(); i++) {
        Instruction *n = nodes[i];
        for (int j : in[n])
            if (j >= 0) g.add(j);
        for (int j : out[n])
            if (j >= 0) g.add(j);

        // page 241
        vector<int> def = n->def();

        for (int a : def) {
            g.add(a);
            for (int b : out[n]) {
                if (b != -1 && a != -1) {
                    g.add_edge(a, b);
                }
            }
        }
    }

    return g;
}

static void spill(InstructionGraph &graph, int var, int delta,
                  int tmp_var) {
    // somenta a divisao no grafo de fluxo se ha jump ou condjump
    // por isso o modo que as coisas sao adicionadas funcionam

    delta = -delta;
    for (Instruction *n : graph.nodes()) {
        vector<int> def = n->def();
        vector<int> use = n->use();
        bool use_var = false;
        bool def_var = false;

        for (int i : def) def_var |= i == var;
        for (int i : use) use_var |= i == var;

        // add three childen
        // int tmp_var = var;

        if (use_var) {
            MoveLiteral *mlit = new MoveLiteral();
            MoveOp *mop = new MoveOp();
            MenToReg *mtor = new MenToReg();

            graph.add(mlit);
            graph.add(mop);
            graph.add(mtor);

            graph.add_edge(mlit, mop);
            graph.add_edge(mop, mtor);

            mlit->value = delta;
            mlit->to = tmp_var;

            mop->operation = PLUS;
            mop->to = tmp_var;
            mop->A = tmp_var;
            mop->B = -1;

            mtor->from = tmp_var;
            mtor->to = tmp_var;

            for (Instruction *from : graph.eddge_from(n)) {
                graph.add_edge(from, mlit);
                graph.remove_edge(from, n);
            }
            graph.add_edge(mtor, n);
            n->change_use(var, tmp_var);
        }

        if (def_var) {
            MoveLiteral *mlit = new MoveLiteral();
            MoveOp *mop = new MoveOp();
            RegToMen *rtom = new RegToMen();

            graph.add(mlit);
            graph.add(mop);
            graph.add(rtom);

            graph.add_edge(mlit, mop);
            graph.add_edge(mop, rtom);

            mlit->value = delta;
            mlit->to = tmp_var;

            mop->operation = PLUS;
            mop->to = tmp_var;
            mop->A = tmp_var;
            mop->B = -1;

            rtom->from = var;
            rtom->to = tmp_var;

            for (Instruction *to : graph.eddge_to(n)) {
                graph.add_edge(rtom, to);
                graph.remove_edge(n, to);
            }
            graph.add_edge(n, mlit);
        }
    }
}

static int numberOfRegister() {
    char *pPath = getenv("MYCC_NUMBER_OF_REGISTERS");
    if (pPath != NULL) return stoi(pPath);
    return NUMBER_OF_REGISTERS;
}

static map<int, int> color(stack<int> stack, Graph<int> g) {
    map<int, int> colors;

    while (!stack.empty()) {
        int e = stack.top();
        stack.pop();

        set<int> temp_colors;
        for (int i = 0; i < numberOfRegister(); ++i)
            temp_colors.insert(i);

        for (int i : g.eddges(e)) {
            if (colors.count(i) != 0) {
                temp_colors.erase(colors[i]);
            }
        }

        if (temp_colors.size() == 0) {
            cout << "FUDEU!";
        }

        colors[e] = *temp_colors.begin();
    }

    return colors;
}

static map<int, int> Main(InstructionGraph &graph, int delta,
                          Ids &ids) {
    VARSET in, out;
    LivenessAnalysis(graph, in, out);

    Graph<int> g = Build(graph, in, out);
    Graph<int> tmp = g;

    // simplify
    int changes;
    stack<int> s;
    do {
        changes = 0;
        vector<int> tmp = g.nodes();

        for (auto i : tmp) {
            // cout << i << " " << g.degree(i) << "\n";
            if (g.degree(i) < numberOfRegister()) {
                s.push(i);
                g.remove(i);
                changes++;
            }
        }
    } while (changes != 0);

    if (g.size() != 0) {
        // select spill
        // lets spill the higher degree
        int max = -1;
        int max_degree = 0;
        vector<int> nodes = g.nodes();
        for (auto i : nodes) {
            if (g.degree(i) > max_degree) {
                max_degree = g.degree(i);
                max = i;
            }
        }

        // g.print();
        cout << "# SPILL %" << max << "\n";
        // cout << delta << "\n";
        spill(graph, max, delta, ids.get("SPILL" + to_string(max)));
        return Main(graph, delta + ids.get_size(max), ids);
    } else {
        map<int, int> colors = color(s, tmp);
        // for (auto i: colors) cout << i.first << " " << i.second <<
        // "\n"; color stack graph.print();
        max_delta = delta;
        return colors;
    }
}

Ids::Ids() {
    str_to_int["$BP"] = -1;
    count = 0;
    sizes.push_back(MEM_POS_SIZE);
}

Ids::~Ids() {}

bool Ids::has(string s) {
    map<string, int>::iterator it = str_to_int.find(s);
    return it != str_to_int.end();
}

int Ids::get(string s) {
    if (!has(s)) {
        str_to_int[s] = count;
        sizes.push_back(0);
        count++;
    }
    return str_to_int[s];
}

int Ids::size() { return count; }

void Ids::set_size(string s, int i) { sizes[str_to_int[s] + 1] = i; }

void Ids::set_size(int id, int i) { sizes[id + 1] = i; }

int Ids::get_size(string s) { return sizes[str_to_int[s] + 1]; }

int Ids::get_size(int i) { return sizes[i + 1]; }
