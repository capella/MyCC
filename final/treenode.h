////////////////////////////////////////////////////////////// */
//
// Author: Gabriel Capella
// Date: 2015-10-13
//
////////////////////////////////////////////////////////////// */
#include <list>
#include <map>
#include <set>
#include <stack>
#include <string>
#include <vector>

#ifndef TREENODE_H
#define TREENODE_H

#include "P.h"
#include "enviroment.h"

class Enviroment;

using namespace std;

class TreeNode {
   private:
    void print(int depth);
    void remove(string name, set<TreeNode *> &s);

   public:
    vector<TreeNode *> nodes;
    TreeNode(int lexid, P::Token token, TreeNode *p);
    TreeNode(int lexid, P::Token token);
    ~TreeNode();

    void add(TreeNode *n);
    void add(TreeNode *n, int i);

    TreeNode *get(P::Token t);

    // return number of childres
    int size();

    // check if tokenstring is  str
    bool is(P::Token str);

    // set tokenstring as str
    void set(P::Token str);

    // check if is the same type
    bool same_type(TreeNode *node);

    // check if has the same content
    bool same_content(TreeNode *node);

    // ser pointer of node at position p
    void set(size_t p, TreeNode *node);

    void remove_token(P::Token name);

    TreeNode *at(unsigned int p);
    void print();
    void clear();

    int id;
    int type;
    Class *clas;

    int lextokenid;
    P::Token token;
    string content;
    Enviroment *env;
    TreeNode *parent;
};

#endif
