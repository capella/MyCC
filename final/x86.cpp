////////////////////////////////////////////////////////////// */
//
// Author: Gabriel Capella
// Date: 2015-10-13
//
////////////////////////////////////////////////////////////// */

#include "x86.h"

static void print_fail() {
    cout << "\n";
    cout << "fail_exit:\n";
    cout << "   movl   $1, %eax\n";
    cout << "   popq   %rbp\n";
    cout << "   retq\n";
}

static void malloc_main(int size) {
    // cout << "   pushq   %rbp           ## MALLOC\n";
    cout << "   movq    $" << size << ", %rax\n";
    cout << "   movq    %rax, %rdi\n";
    cout << "   callq   " << MALLOC_NAME << "\n";
    cout << "   testq   %rax, %rax\n";
    cout << "   jz      fail_exit\n";
    // cout << "   popq    %rbp\n";
}

static stack<Instruction *> init_instructions(
    DiGraph<Instruction *> &b) {
    stack<Instruction *> begin;
    for (auto i : b.nodes()) {
        if (b.eddge_from(i).size() == 0) begin.push(i);
    }
    return begin;
}

void build(map<int, int> &colors, DiGraph<Instruction *> &graph,
           int mainsize, int max_delta) {
    map<int, string> names;
    for (auto i : colors) {
        names[i.first] = registers_names[i.second];
    }
    names[-1] = registers_names[-1];

    // maloc main
    cout << ".globl " << MAIN_NAME << "\n";
    cout << "\n";
    cout << "" << MAIN_NAME << ":\n";
    cout << "   pushq  %rbp                # Save old EBP\n";
    cout << "   movq    %rsp, %rbp         # Save old ESP\n\n";

    malloc_main(mainsize);
    cout << "   pushq   %rax\n\n";  // pop malloc value

    cout << "   callq   __LABEL0\n";

    cout << "   popq   %rax\n\n";

    cout << "   movq %rbp, %rsp            # Restore ESP\n";
    cout << "   popq %rbp                  # Restore EBP\n";
    cout << "   retq\n";

    stack<Instruction *> s = init_instructions(graph);
    // int count = s.size();

    set<Instruction *> visited;
    while (!s.empty()) {
        Instruction *inst = s.top();
        s.pop();

        if (!dynamic_cast<Label *>(inst))
            cout << "   ##" << inst->name() << "\n";
        cout << inst->asm_str(names);

        if (graph.eddge_from(inst).size() == 0) {
            cout << "   pushq  %rbp                # Save old EBP\n";
            cout << "   movq    %rsp, %rbp         # Save old ESP\n";
            cout << "   addq    $-" + to_string(max_delta) +
                        ", %rsp\n\n";
        }

        if (graph.eddge_to(inst).size() == 0) {
            cout << "\n   movq    %rbp, %rsp            # Restore "
                    "ESP\n";
            cout
                << "   popq    %rbp                  # Restore EBP\n";
            cout << "   retq\n";
        }

        if (JumpCond *j = dynamic_cast<JumpCond *>(inst)) {
            for (auto next : graph.eddge_to(inst)) {
                if (Label *l = dynamic_cast<Label *>(next)) {
                    if (j->to == l->url && visited.count(next) == 0) {
                        s.push(next);
                        visited.insert(next);
                    }
                }
            }
            for (auto next : graph.eddge_to(inst)) {
                if (Label *l = dynamic_cast<Label *>(next)) {
                    if (j->to != l->url && visited.count(next) == 0) {
                        s.push(next);
                        visited.insert(next);
                    }
                }
            }
        } else
            for (auto next : graph.eddge_to(inst)) {
                if (visited.count(next) == 0) {
                    s.push(next);
                    visited.insert(next);
                }
            }
    }

    print_fail();
}
