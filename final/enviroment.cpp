////////////////////////////////////////////////////////////// */
//
// Author: Gabriel Capella
// Date: 2015-10-13
//
////////////////////////////////////////////////////////////// */
#include "enviroment.h"

Enviroment::Enviroment() {
    upper = NULL;

    f = NULL;
    c = NULL;
}

Enviroment::~Enviroment() {
    // free(this);
}

bool Enviroment::addvar(string value, Variable *type) {
    pair<map<string, Variable *>::iterator, bool> ret;

    ret = variables.insert(pair<string, Variable *>(value, type));

    return ret.second;
}

bool Enviroment::hasvar(string value) {
    Enviroment *env = this;
    while (env != NULL) {
        map<string, Variable *>::iterator it;
        it = env->variables.find(value);
        if (it != env->variables.end()) return true;
        env = env->upper;
    }
    return false;
}

Variable *Enviroment::getvar(string value) {
    Enviroment *env = this;
    while (env != NULL) {
        map<string, Variable *>::iterator it;
        it = env->variables.find(value);
        if (it != env->variables.end()) return it->second;
        env = env->upper;
    }
    return NULL;
}

int Enviroment::size() {
    int size = 0;
    f = NULL;
    c = NULL;
    map<string, Variable *>::iterator it;

    for (it = variables.begin(); it != variables.end(); ++it) {
        size += getsize(it->second->type);
    }

    // cout << variables.size() << "\n";

    if (upper == NULL) return size;
    return size + upper->size();
}

Enviroment *Enviroment::clone() {
    Enviroment *n = new Enviroment();
    n->upper = this;
    n->f = f;
    n->c = c;
    return n;
}

Function::Function() {
    args_size = 0;
    position = 0;
    return_class = NULL;
    has_return_declared = false;
    return_type = NONE;
    statment = NULL;
}

Function::~Function() {}

void Function::addarg(string name, Variable *v) {
    vars.push_back(v);
    vars_position[name] = position;
    args_size += v->size;
    position++;
}

bool Function::hasarg(string value) {
    map<string, int>::iterator it;
    it = vars_position.find(value);
    if (it != vars_position.end()) return true;
    return false;
}

bool Class::hasmet(string value) {
    map<string, Function *>::iterator it;
    it = methods.find(value);
    if (it != methods.end()) return true;
    return false;
}

bool Class::hasvar(string value) {
    map<string, Variable *>::iterator it;
    it = variables.find(value);
    if (it != variables.end()) return true;
    return false;
}

Variable::Variable(int type, Class *c) {
    this->type = type;
    if (type == INT) size = INT_SIZE;
    if (type == BOOL) size = BOOL_SIZE;
    if (type == POINTER) size = POINTER_SIZE;
    this->c = c;
}

Variable::~Variable() {
    // free(this);
}

int getsize(int type) {
    if (type == BOOL) return BOOL_SIZE;
    if (type == INT) return INT_SIZE;
    if (type == POINTER) return POINTER_SIZE;
    return -1;
}

int Enviroment::sizevar(string value) {
    if (hasvar(value))
        return upper->sizevar(value);
    else
        return size();
}

Class::Class() { size = 0; }

Class::~Class() {}

void Class::print() {}

void Class::addvar(string name, Variable *v) {
    // cout << name << "<<";
    variables_pos[name] = size;
    size += v->size;
    variables[name] = v;
}

void Class::addmethod(string name, Function *f) { methods[name] = f; }
