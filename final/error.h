////////////////////////////////////////////////////////////// */
//
// Author: Gabriel Capella
// Date: 2015-10-13
//
////////////////////////////////////////////////////////////// */
#include <iostream>
#include <string>
#include <vector>

#ifndef ERROR_H_
#define ERROR_H_
#include "lex.h"

using namespace std;

class Error {
   private:
    Lex *lex;
    string *code;
    vector<int> positions;
    vector<string> msgs;
    void print(int x);

   public:
    Error(Lex *lex, string *code);
    ~Error();
    void add(int position, string msg);
    void printall();
    int size();
};

#endif
