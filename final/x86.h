////////////////////////////////////////////////////////////// */
//
// Author: Gabriel Capella
// Date: 2015-10-13
//
////////////////////////////////////////////////////////////// */
#include <algorithm>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <stack>
#include <string>
#include <vector>

#ifndef X86_H
#define X86_H

#include "enviroment.h"
#include "error.h"
#include "instruction.h"
#include "toinstructions.h"
#include "treenode.h"

using namespace std;

static map<int, string> registers_names{
    {-1, "%rbp"}, {0, "%rax"},  {1, "%rbx"}, {2, "%rcx"}, {3, "%rdx"},
    {4, "%r8"},   {5, "%r9"},   {6, "%r10"}, {7, "%r11"}, {8, "%r12"},
    {9, "%r13"},  {10, "%r14"}, {11, "%r15"}};

void build(map<int, int> &colors, DiGraph<Instruction *> &graph,
           int mainsize, int max_delta);

#endif
