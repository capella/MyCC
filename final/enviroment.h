////////////////////////////////////////////////////////////// */
//
// Author: Gabriel Capella
// Date: 2015-10-13
//
////////////////////////////////////////////////////////////// */
#include <list>
#include <map>
#include <stack>
#include <string>
#include <vector>

#include "treenode.h"

#ifndef ENVIROMENT_H
#define ENVIROMENT_H

#define INT_SIZE 8
#define BOOL_SIZE 8
#define POINTER_SIZE 8

using namespace std;
class TreeNode;
class Class;

enum types { BOOL, INT, POINTER, VOID, NONE };

class Variable {
   public:
    Class *c;
    Variable(int type, Class *c);
    ~Variable();
    int type;
    int size;
};

class Function {
   private:
    int position;

   public:
    Function();
    ~Function();

    vector<Variable *> vars;
    map<string, int> vars_position;
    int args_size;
    Class *return_class;

    int return_type;
    bool has_return_declared;
    void addarg(string name, Variable *v);
    bool hasarg(string name);
    TreeNode *statment;
    // int size;
};

class Class {
   public:
    Class();
    ~Class();

    map<string, Function *> methods;
    map<string, Variable *> variables;
    map<string, int> variables_pos;

    void addvar(string name, Variable *v);
    void addmethod(string name, Function *f);
    bool hasmet(string value);
    bool hasvar(string value);

    int size;
    void print();
};

class Enviroment {
   public:
    Enviroment();
    ~Enviroment();
    // name, type
    map<string, Variable *> variables;
    Function *f;
    Class *c;

    bool addvar(string value, Variable *type);
    bool hasvar(string value);
    // bool addfun (string value, Function * func);
    // bool hasfun (string value);
    // Function * getfun (string value);
    Variable *getvar(string value);

    Enviroment *clone();
    int size();
    int sizevar(string value);
    Enviroment *upper;
};

int getsize(int type);

#endif
