////////////////////////////////////////////////////////////// */
//
// Author: Gabriel Capella
// Date: 2015-10-13
//
////////////////////////////////////////////////////////////// */
#include "treenode.h"
#include "P.h"

#include <iostream>

static int count_codes = 0;

TreeNode::TreeNode(int lexid, P::Token token, TreeNode *p)
    : lextokenid(lexid), token(token), parent(p) {
    count_codes++;
    id = count_codes;
    env = NULL;
    type = NONE;
    clas = NULL;
}

TreeNode::~TreeNode() {
    for (int i = 0; i < size(); i++) {
        delete at(i);
    }
    // free(this);
}

TreeNode::TreeNode(int lexid, P::Token token)
    : lextokenid(lexid), token(token) {
    count_codes++;
    id = count_codes;
    env = NULL;
    parent = NULL;
    type = NONE;
}

void TreeNode::add(TreeNode *n) {
    if (n == NULL) return;
    if (n->env == NULL) n->env = env;
    n->parent = this;
    nodes.push_back(n);
}

void TreeNode::add(TreeNode *n, int i) {
    if (n == NULL) return;
    if (n->env == NULL) n->env = env;
    n->parent = this;

    std::vector<TreeNode *>::iterator it;
    it = nodes.begin();
    it = nodes.insert(it + i, n);
}

TreeNode *TreeNode::get(P::Token t) {
    for (int i = nodes.size() - 1; i >= 0; i--) {
        if (nodes[i] == NULL) continue;
        if (nodes[i]->token == t) return nodes[i];
    }
    return NULL;
}

int TreeNode::size() { return nodes.size(); }

bool TreeNode::is(P::Token t) { return t == token; }

void TreeNode::set(P::Token t) { token = t; }

void TreeNode::set(size_t p, TreeNode *node) {
    if (node == NULL) return;
    if (node->env == NULL) node->env = env;

    nodes[p] = node;
    node->parent = this;
}

bool TreeNode::same_type(TreeNode *node) { return is(node->token); }

bool TreeNode::same_content(TreeNode *node) {
    return !content.compare(node->content);
}

TreeNode *TreeNode::at(unsigned int p) {
    if (p >= nodes.size()) return NULL;
    return nodes[p];
}

void TreeNode::print(int depth) {
    cout << id << " [ ";

    if (env) {
        cout << "style=\"filled\" ";
        cout << "fillcolor=\"";
        cout << (((long)env) % 90) / 90.0 << " ";
        cout << (((long)env) % 90) / 90.0;
        cout << " 0.9 \" ";
    }

    cout << "label = \"" << P::TOKEN_TO_STRING[this->token];

    // print enviroment
    cout << '\n';
    cout << content;
    if (getsize(type) >= 0) {
        cout << '\n';
        cout << getsize(type);
    }

    cout << "\" ];\n";
    for (int i = size() - 1; i >= 0; i--) {
        TreeNode *k = nodes[i];
        if (k == NULL) continue;
        cout << id << " -- " << k->id << ";\n";
        k->print(depth + 1);
    }
}

void TreeNode::print() {
    printf("graph G {\n");
    print(0);
    printf("}\n");
}

void TreeNode::remove_token(P::Token name) {
    int j = 0;
    int i;

    for (i = 0; i < size(); i++) {
        set(j, at(i));
        if (name != at(i)->token) j++;
    }
    nodes.resize(j);
    for (i = 0; i < size(); i++) at(i)->remove_token(name);
}

void TreeNode::clear() { nodes.clear(); }
