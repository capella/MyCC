FROM alpine:3.8

RUN apk add --no-cache g++
RUN apk add --update make
RUN apk add bash
RUN apk add perl-utils

COPY . .

RUN make clean

RUN make

RUN make test
