#!/bin/bash

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"
INIT_TEST="================= START SIMULATION =============="
END_TEST="================== END SIMULATION ==============="

function red() {
    printf "\e[31m$1\e[0m"
}

function green() {
    printf "\e[32m$1\e[0m"
}

function get_result() {
    sed -n -e "/$INIT_TEST/,\$p" $1 | tail -n +2 >/tmp/$hash
    sed -e "/$END_TEST/,\$d" /tmp/$hash >$2
    rm /tmp/$hash
}

valgrinTest=0
while getopts ":v" opt; do
    case $opt in
    v)
        valgrinTest=1
        ;;
    *)
        continue
        ;;
    esac
done

for arg; do
    shift
    [ "$arg" = "-v" ] && continue
    set -- "$@" "$arg"
done

# This file will run tests in tests describe in tests
# set -e
status=0

cd $DIR
cd ..
make
cd $DIR
cd ../programs

function test() {
    export MYCC_NUMBER_OF_REGISTERS=$1
    shift 1
    for file in $@; do
        file=$(basename $file)

        info=(${file//./ })
        name=${info[0]}

        printf "%-20s Running" "$name:"

        hash=$(echo $name | shasum | cut -d" " -f1 -)

        if [ $valgrinTest != 1 ]; then
            ../mycc $name.mycc >/tmp/mycc_test_$hash.s
        else
            valgrind --leak-check=full ../mycc $name.mycc >/tmp/mycc_test_$hash.s
        fi

        if [ $? -eq 1 ]; then
            printf "\b\b\b\b\b\b\b"
            printf "       "
            printf "\b\b\b\b\b\b\b"
            red "ERROR in compilation!\n"
            status=1
            continue
        fi
        cat /tmp/mycc_test_$hash.s >$name.s

        gcc /tmp/mycc_test_$hash.s -o /tmp/mycc_$hash 2>/tmp/mycc_error_$hash
        if [ $? -eq 1 ]; then
            printf "\b\b\b\b\b\b\b"
            printf "       "
            printf "\b\b\b\b\b\b\b"
            red "ERROR in assembly!\n"
            cat /tmp/mycc_error_$hash
            cat /tmp/mycc_test_$hash.s
            status=1
            continue
        fi

        printf "\b\b\b\b\b\b\b"
        printf "       "
        printf "\b\b\b\b\b\b\b"

        { /tmp/mycc_$hash; } &>/tmp/mycc_result_$hash

        result=$(diff -B /tmp/mycc_result_$hash $name.test)

        if [ $? -eq 0 ]; then
            green "OK\n"
        else
            red "Different output\n"
            diff -B /tmp/mycc_result_$hash $name.test
            status=1
        fi

        rm /tmp/mycc_*
    done
}

for i in {2..12}; do
    printf "\nNumber o register = $i\n"
    test $i $@
done

exit $status
