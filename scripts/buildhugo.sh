#!/bin/bash

index=./site/content/_index.md
cp README.md $index

printf "### Tests:" >>$index
printf "\n\n" >>$index
echo "Program | 2 Registers | 6 Registers | 12 Registers" >>$index

echo "--- | :---: | :---: | :---: " >>$index
index=../site/content/_index.md

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"

# This file will run tests in tests describe in tests
# set -e
status=0

cd $DIR
cd ..
make

cd $DIR
cd ../programs
pwd
dot -V

for file in $@; do
    file=$(basename $file)

    for i in {2,6,12}; do
        export MYCC_NUMBER_OF_REGISTERS=$i

        info=(${file//./ })
        name=${info[0]}
        backname=$name
        name="$MYCC_NUMBER_OF_REGISTERS$name"
        hash=$(echo $name | shasum | cut -d" " -f1 -)

        if [ $i -eq 2 ]; then
            printf "./$backname" >>$index
        fi
        printf " | " >>$index

        md=../site/content/code/$name.md
        mkdir -p $(dirname $md)
        touch $md
        static=../site/static
        echo "---" >$md
        echo "title: $name" >>$md
        echo "---" >>$md

        echo "### Source Code" >>$md
        echo "\`\`\`java" >>$md
        cat $backname.mycc >>$md
        echo "\`\`\`" >>$md

        for mode in $(grep -o . <<<"sbklniyx"); do
            mkdir -p $static
        done
        for mode in $(grep -o . <<<"sbklniyx"); do
            ../mycc $backname.mycc -$mode | dot -Tpng -Gdpi=70 >"$static/$hash-$mode.png" &
        done
        wait
        for mode in $(grep -o . <<<"sbklniyx"); do
            echo "" >>$md
            echo "![preview](../$hash-$mode.png)" >>$md
            echo "" >>$md
        done

        echo "### Results" >>$md

        ../mycc $backname.mycc >/tmp/mycc_test_$hash.s

        if [ $? -eq 1 ]; then
            echo "#### Erro in compilation" >>$md
            echo "\`\`\`" >>$md
            cat /tmp/mycc_test_$hash.s >>$md
            echo "\`\`\`" >>$md
            printf "[&#10007;]($name)" >>$index
            if [ $i -eq 12 ]; then
                printf " \n" >>$index
            fi
            continue
        fi
        cat /tmp/mycc_test_$hash.s >$name.s

        echo "#### Assembly" >>$md
        echo "" >>$md
        echo "\`\`\`nasm" >>$md
        cat /tmp/mycc_test_$hash.s >>$md
        echo "\`\`\`" >>$md

        gcc /tmp/mycc_test_$hash.s -o /tmp/mycc_$hash 2>/tmp/mycc_error_$hash
        if [ $? -eq 1 ]; then
            echo "#### Erro in compilation" >>$md
            echo "\`\`\`" >>$md
            cat /tmp/mycc_error_$hash >>$md
            echo "\`\`\`" >>$md
            echo "\`\`\`" >>$md
            cat /tmp/mycc_test_$hash.s >>$md
            echo "\`\`\`" >>$md
            printf "[&#10007;]($name)" >>$index
            if [ $i -eq 12 ]; then
                printf " \n" >>$index
            fi
            continue
        fi

        { /tmp/mycc_$hash; } &>/tmp/mycc_result_$hash

        echo "##### Expected" >>$md
        echo "" >>$md
        echo "\`\`\`" >>$md
        cat $backname.test >>$md
        printf "\n" >>$md
        echo "\`\`\`" >>$md

        echo "##### Got" >>$md
        echo "" >>$md
        echo "\`\`\`" >>$md
        cat /tmp/mycc_result_$hash >>$md
        printf "\n" >>$md
        echo "\`\`\`" >>$md

        result=$(diff -B /tmp/mycc_result_$hash $backname.test)

        if [ $? -eq 0 ]; then
            printf "[&#10003;]($name)" >>$index
            if [ $i -eq 12 ]; then
                printf " \n" >>$index
            fi
        else
            printf "[&#10007;]($name)" >>$index
            if [ $i -eq 12 ]; then
                printf " \n" >>$index
            fi
        fi

        # rm /tmp/smart_bash_$hash
        rm /tmp/mycc_*
    done
done

cd $DIR
cd ../site
hugo -v
