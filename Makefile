all:
	$(MAKE)  -C lex
	$(MAKE)  -C parser
	$(MAKE)  -C final
	cp final/mycc mycc
	@echo "Finished!"

test: all
	./scripts/test.sh ./programs/*.mycc

clean:
	echo "> Cleaning final"
	$(MAKE) -C final clean
	echo "> Cleaning lex"
	$(MAKE)  -C lex clean
	echo "> Cleaning parser"
	$(MAKE)  -C parser clean
	$(RM) mycc

img: all
	./mycc programs/main.mycc -s | dot -Tpng   | imgcat

check:
	cppcheck --enable=all --quiet .

style:
	make clean
	clang-format -i  */**.cpp */**.h */**.hpp

.SILENT:
